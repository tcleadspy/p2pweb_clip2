﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(p2pweb_clip2.Startup))]
namespace p2pweb_clip2
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
