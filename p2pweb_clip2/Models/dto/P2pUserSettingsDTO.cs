﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace p2pweb_clip2.Models.dto
{
    public class P2pUserSettingsDTO
    {
        public string username { get; set; }
        public List<string> keywords { get; set; }
        public List<FBPageJsonDTO> pagewatches { get; set; }
        public List<FBPageDTO> groupwatches { get; set; }

        public bool facebookIntegration { get; set; }
        public string url4msgs { get; set; }
        public List<SentMessageDTO> sentmsgs { get; set; }
        public string appid { get; set; }
    }
}