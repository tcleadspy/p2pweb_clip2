﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace p2pweb_clip2.Models.dto
{
    public class SentMessageDTO
    {
        public string rcpt_fbid { get; set; }
        public DateTime sent_date { get; set; }
    }
}