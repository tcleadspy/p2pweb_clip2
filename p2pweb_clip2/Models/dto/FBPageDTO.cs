﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace p2pweb_clip2.Models.dto
{
    public class FBPageDTO
    {
        public string feedid { get; set; }
        public string name { get; set; }
        public string category { get; set; }
        public int fan_count { get; set; }
    }

    public class FBPageJsonDTO
    {
        public string id { get; set; }
        public string objJson { get; set; }
        public string feedType { get; set; }
    }
}