﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using p2pweb_clip2.Models.edm;

namespace p2pweb_clip2.Models.dto
{
    public class UserOptionsDTO
    {
        public bool fbincorporated { get; set; }
        public string url4msgs { get; set; }

        public UserOptionsDTO(UserOption dbObj)
        {
            this.fbincorporated = dbObj.fb_incorporation_on;
            this.url4msgs = dbObj.url;
        }

        /* parameterless constructor needed for serialization */
        public UserOptionsDTO()
        {

        }
    }

}