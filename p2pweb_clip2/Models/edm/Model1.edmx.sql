
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 05/06/2022 22:55:20
-- Generated from EDMX file: C:\src\angular\p2pweb_clip2\p2pweb_clip2\Models\edm\Model1.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [pouncedb];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------


-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[AspNetUsers]', 'U') IS NOT NULL
    DROP TABLE [dbo].[AspNetUsers];
GO
IF OBJECT_ID(N'[dbo].[FeedBlobs]', 'U') IS NOT NULL
    DROP TABLE [dbo].[FeedBlobs];
GO
IF OBJECT_ID(N'[dbo].[GroupBlobs]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GroupBlobs];
GO
IF OBJECT_ID(N'[dbo].[GroupWatches]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GroupWatches];
GO
IF OBJECT_ID(N'[dbo].[Keywords]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Keywords];
GO
IF OBJECT_ID(N'[dbo].[log]', 'U') IS NOT NULL
    DROP TABLE [dbo].[log];
GO
IF OBJECT_ID(N'[dbo].[PageWatches]', 'U') IS NOT NULL
    DROP TABLE [dbo].[PageWatches];
GO
IF OBJECT_ID(N'[dbo].[SentMessages]', 'U') IS NOT NULL
    DROP TABLE [dbo].[SentMessages];
GO
IF OBJECT_ID(N'[dbo].[UserOptions]', 'U') IS NOT NULL
    DROP TABLE [dbo].[UserOptions];
GO
IF OBJECT_ID(N'[ModelStoreContainer].[Feeds]', 'U') IS NOT NULL
    DROP TABLE [ModelStoreContainer].[Feeds];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'AspNetUsers'
CREATE TABLE [dbo].[AspNetUsers] (
    [Id] nvarchar(128)  NOT NULL,
    [Hometown] nvarchar(max)  NULL,
    [Email] nvarchar(256)  NULL,
    [EmailConfirmed] bit  NOT NULL,
    [PasswordHash] nvarchar(max)  NULL,
    [SecurityStamp] nvarchar(max)  NULL,
    [PhoneNumber] nvarchar(max)  NULL,
    [PhoneNumberConfirmed] bit  NOT NULL,
    [TwoFactorEnabled] bit  NOT NULL,
    [LockoutEndDateUtc] datetime  NULL,
    [LockoutEnabled] bit  NOT NULL,
    [AccessFailedCount] int  NOT NULL,
    [UserName] nvarchar(256)  NOT NULL
);
GO

-- Creating table 'FeedBlobs'
CREATE TABLE [dbo].[FeedBlobs] (
    [id] nvarchar(50)  NOT NULL,
    [obj] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'GroupBlobs'
CREATE TABLE [dbo].[GroupBlobs] (
    [id] nvarchar(50)  NOT NULL,
    [obj] nvarchar(max)  NULL
);
GO

-- Creating table 'GroupWatches'
CREATE TABLE [dbo].[GroupWatches] (
    [id] bigint IDENTITY(1,1) NOT NULL,
    [username] nvarchar(256)  NOT NULL,
    [feedid] nvarchar(50)  NOT NULL
);
GO

-- Creating table 'Keywords'
CREATE TABLE [dbo].[Keywords] (
    [id] bigint IDENTITY(1,1) NOT NULL,
    [username] nvarchar(256)  NOT NULL,
    [keyword1] nvarchar(50)  NOT NULL
);
GO

-- Creating table 'PageWatches'
CREATE TABLE [dbo].[PageWatches] (
    [id] bigint IDENTITY(1,1) NOT NULL,
    [username] nvarchar(256)  NOT NULL,
    [feedid] nvarchar(50)  NOT NULL
);
GO

-- Creating table 'Feeds'
CREATE TABLE [dbo].[Feeds] (
    [feedid] nvarchar(50)  NOT NULL,
    [name] nvarchar(250)  NOT NULL,
    [description] nvarchar(250)  NULL,
    [category] nvarchar(100)  NULL,
    [fan_count] int  NOT NULL,
    [about] nvarchar(250)  NULL
);
GO

-- Creating table 'UserOptions'
CREATE TABLE [dbo].[UserOptions] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [username] nvarchar(256)  NOT NULL,
    [fb_incorporation_on] bit  NOT NULL,
    [url] varchar(50)  NULL
);
GO

-- Creating table 'logs'
CREATE TABLE [dbo].[logs] (
    [id] int IDENTITY(1,1) NOT NULL,
    [msg] varchar(max)  NOT NULL
);
GO

-- Creating table 'SentMessages'
CREATE TABLE [dbo].[SentMessages] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [username] nvarchar(256)  NOT NULL,
    [rcpt_fbid] nvarchar(100)  NOT NULL,
    [sent_date] datetime  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'AspNetUsers'
ALTER TABLE [dbo].[AspNetUsers]
ADD CONSTRAINT [PK_AspNetUsers]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [id] in table 'FeedBlobs'
ALTER TABLE [dbo].[FeedBlobs]
ADD CONSTRAINT [PK_FeedBlobs]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'GroupBlobs'
ALTER TABLE [dbo].[GroupBlobs]
ADD CONSTRAINT [PK_GroupBlobs]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'GroupWatches'
ALTER TABLE [dbo].[GroupWatches]
ADD CONSTRAINT [PK_GroupWatches]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'Keywords'
ALTER TABLE [dbo].[Keywords]
ADD CONSTRAINT [PK_Keywords]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'PageWatches'
ALTER TABLE [dbo].[PageWatches]
ADD CONSTRAINT [PK_PageWatches]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [feedid], [name], [fan_count] in table 'Feeds'
ALTER TABLE [dbo].[Feeds]
ADD CONSTRAINT [PK_Feeds]
    PRIMARY KEY CLUSTERED ([feedid], [name], [fan_count] ASC);
GO

-- Creating primary key on [Id] in table 'UserOptions'
ALTER TABLE [dbo].[UserOptions]
ADD CONSTRAINT [PK_UserOptions]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [id] in table 'logs'
ALTER TABLE [dbo].[logs]
ADD CONSTRAINT [PK_logs]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [Id] in table 'SentMessages'
ALTER TABLE [dbo].[SentMessages]
ADD CONSTRAINT [PK_SentMessages]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------