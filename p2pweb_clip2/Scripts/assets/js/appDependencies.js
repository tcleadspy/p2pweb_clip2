/** 
  * declare 'clip-two' module with dependencies
*/
'use strict';
angular.module("clip-two", [
	'ngAnimate',
	'ngCookies',
	'ngStorage',
	'ngSanitize',
	'ngTouch',
	'ui.router',
	'ui.bootstrap',
	'oc.lazyLoad',
    'angular-loading-bar',
    'angular.filter',
	'ncy-angular-breadcrumb',
	'duScroll',
	'pascalprecht.translate'
]);