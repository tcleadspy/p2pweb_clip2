'use strict';
/**
 * Clip-Two Main Controller
 */
app.controller('AppCtrl', ['$rootScope', '$scope', '$state', '$translate', '$localStorage', '$window', '$document', '$timeout', 'cfpLoadingBar', '$transitions', 'cacheSvc', 'fbookSvc', 'common',
    function ($rootScope, $scope, $state, $translate, $localStorage, $window, $document, $timeout, cfpLoadingBar, $transitions, cacheSvc, fbookSvc, common) {

        // Loading bar transition
        // -----------------------------------
        var $win = $($window);

        $scope.loaded = false
        $scope.maxCacheAgeInHrs = 24
        $scope.percentFinished = 10
        $scope.statusmsg = "Initializing"
        $rootScope.pageSize = 10

        $rootScope.updateStatus = function (msg) {
            if ($rootScope.$$phase) {
                $scope.statusmsg = msg 
            } else {
                $rootScope.$apply(function () { $scope.statusmsg = msg })
            }
        }

        $rootScope.navElements = [
            { sref: "app.facebookfeeds", icon: "ti-facebook", fa_icon: "ti-facebook", titleTranslate: "sidebar.nav.element.FACEBOOK", defaultText: "Feeds", desc: 'Add or remove Facebook feeds' },
            { sref: "app.keywords", icon: "ti-key", fa_icon: "fa-key", titleTranslate: "sidebar.nav.element.KEYWORDS", defaultText: "Keywords", desc: 'Add or remove default keywords' },
            { sref: "app.mining", icon: "ti-search", fa_icon: "fa-search", titleTranslate: "sidebar.nav.element.MINING", defaultText: "Mine", desc: 'Mine the data, find matching comments, contact prospects' },
            { sref: "app.leadspyview", icon: "ti-desktop", fa_icon: "fa-desktop", titleTranslate: "sidebar.nav.element.RETRO", defaultText: "Retro View", desc: 'Choose feeds to mine, find matching comments, contact prospects'  },
            { sref: "app.starred", icon: "ti-star", fa_icon: "fa-star", titleTranslate: "sidebar.nav.element.STARRED", defaultText: "Starred Comments", desc: 'Review your starred comments' },
        ]

        $rootScope.updatePageSize = function (newPageSize) {
            $rootScope.pageSize = newPageSize
        }
        $rootScope.logEvent = function (rootscope, scope) {
            rootscope.updatePageSize(scope.tableParams.count())
        }

        /* update 'loaded' flag */
        $scope.setReady = function (loaded) {
            $scope.$apply(function () { $scope.loaded = loaded })
        }

        /* determine feeds to delete and feeds to download */
        $scope.getFeedCleanupInfo = function (feeds, pagewatches) {
            var rightNow = new Date()
            var cutoffDate = rightNow.setHours(rightNow.getHours() - $scope.maxCacheAgeInHrs)

            /* determine expired or orphaned feeds to delete from cache */
            var feeds2Delete = feeds.filter(function (feed) {
                return (
                    Date.parse(feed.timestamp) < cutoffDate
                    || common.searchArrayForID(pagewatches, feed.id) === -1
                )
            })

            /* determine expired, non-orphaned feeds which will need to be refreshed */
            var feeds2Refresh = feeds.filter(function (feed) {
                return (
                    Date.parse(feed.timestamp) < cutoffDate
                    && common.searchArrayForID(pagewatches, feed.id) !== -1
                )
            })

            /* determine if we have missing cache entries. this can happen if using multiple devices */
            var feedsMissing = pagewatches.filter(function (pagewatch) {
                return (common.searchArrayForID(feeds, pagewatch.id) === -1)
            })


            /* combine list of feeds that are missing, and list of feeds needing refreshing */
            var feeds2Download = feedsMissing.concat(feeds2Refresh)


            return {
                feeds2Delete: feeds2Delete
                , feeds2Download: feeds2Download
            }

        }

        $scope.setwatchcachetimestamp = function (watches, feed) {
            var correspondingIdx = common.searchArrayForID($rootScope.appDataObj.feedsSelected, feed.id)
            if (correspondingIdx === -1) return false

            var correspondingObj = watches[correspondingIdx]
            correspondingObj.timestamp = Date.parse(feed.timestamp)
            return true
        }

        /* download the feed data for an array of facebook feeds */
        $scope.downloadFeeds = function (feeds2Download) {

            /* determine if we have feeds to download */
            $rootScope.updateStatus("# feeds to download: " + feeds2Download.length)
            if (feeds2Download.length === 0) {
                /* no feeds to download, update 'loaded' flag */
                $scope.setReady(true)
                //$scope.$apply(function () { $scope.percentFinished = 100 })
                $scope.percentFinished = 100
                return
            }

            /*syncing hack to know when done downloading */
            var numExpected = feeds2Download.length
            var origNumExpected = numExpected

            /* begin downloads */
            $scope.percentFinished = 80
            feeds2Download.forEach(function (feed) {

                /* download one feed */
                $rootScope.updateStatus("downloading feed: " + feed.name)
                fbookSvc.getFeed(feed.id, function (data) {

                    /* 
                    null data maybe needs to be handled differently
                    if we simply don't save it, then we'll re-download every time
                    */
                    if (data !== null) {
                        /* save feed contents to local cache */
                        $rootScope.updateStatus("saving feed: " + feed.name + " to cache")
                        $rootScope.cache.saveFeed(new feedCacheEntry(feed, data))
                        $scope.setwatchcachetimestamp($rootScope.appDataObj.feedsSelected, feed)
                    }

                    /* check if we are done */
                    numExpected--

                    $scope.$apply(function () {
                        $scope.percentFinished = Math.round((origNumExpected - (origNumExpected - numExpected)) / origNumExpected)
                    })

                    if (numExpected === 0) {

                        /* done */
                        $rootScope.updateStatus("done with feed downloads...")
                        $scope.setReady(true)
                    }

                })
            })
        }

        /* open indexeddb (async) */
        $rootScope.statusmsg = "opening feed cache.."
        $rootScope.cache = cacheSvc
        cacheSvc.initialize(function () {

            /* take the initializer passed from the view via a window property */
            var results = window.p2pinitializer

            //$rootScope.$apply(function () {
                $rootScope.user.name = results["username"]
                $rootScope.appDataObj.keywords = results["keywords"]
                $rootScope.appDataObj.feedsSelected = results["pagewatches"].map(function (pw) {
                    const obj = JSON.parse(pw.objJson)
                    obj.busy = false
                    obj.feedType = pw.feedType
                    return obj
                })
                $rootScope.appDataObj.fbincorporated = (results["fbincorporated"].toString().toLowerCase() === "true")
                $rootScope.appDataObj.url4msgs = results["url4msgs"]

                /* load the cache to see which need refreshing */
                $rootScope.statusmsg = "loading feed cache.."
                $rootScope.cache.loadCache(function (feeds) {

                    feeds.forEach(function (feed) {
                        var result = $scope.setwatchcachetimestamp($rootScope.appDataObj.feedsSelected, feed)

                        if (!result) {
                            $rootScope.cache.removeFeed(feed.id)
                        }
                    })

                    var cleanupInfo = $scope.getFeedCleanupInfo(feeds, $rootScope.appDataObj.feedsSelected)
                    var feeds2Delete = cleanupInfo.feeds2Delete

                    /* delete feeds from cache */
                    $rootScope.updateStatus("# of feeds to delete from cache: " + feeds2Delete.length)
                    console.log('# of feeds to delete from cache: ' + feeds2Delete.length)
                    feeds2Delete.forEach(function (feed) {
                        $rootScope.cache.removeFeed(feed.id)
                    })

                    /* checkfbloaded polls for facebook initialization to complete */
                    $rootScope.updateStatus("checking facebook api status...")
                    common.checkfbloaded(function () {
                        /* facebook initialization complete */
                        $rootScope.updateStatus("facebook api loaded...")
                        $scope.downloadFeeds(cleanupInfo.feeds2Download)
                    })
                })
        })


        $transitions.onStart({}, function (trans) {
            cfpLoadingBar.start();
            if (typeof CKEDITOR !== 'undefined') {
                for (name in CKEDITOR.instances) {
                    CKEDITOR.instances[name].destroy();
                }
            }
        });

        $transitions.onSuccess({}, function (trans) {
            //stop loading bar on stateChangeSuccess
            $scope.$on('$viewContentLoaded', function (event) {
                cfpLoadingBar.complete();
            });

            // scroll top the page on change state
            $('#app .main-content').css({
                position: 'relative',
                top: 'auto'
            });

            $('footer').show();

            window.scrollTo(0, 0);

            if (angular.element('.email-reader').length) {
                angular.element('.email-reader').animate({
                    scrollTop: 0
                }, 0);
            }

            // Save the route title
            $rootScope.currTitle = $state.current.title;
        });

        $rootScope.pageTitle = function () {
            return $rootScope.app.name + ' - ' + ($rootScope.currTitle || $rootScope.app.description);
        };

        // save settings to local storage
        if (angular.isDefined($localStorage.layout)) {
            $scope.app.layout = $localStorage.layout;

        } else {
            $localStorage.layout = $scope.app.layout;
        }
        $scope.$watch('app.layout', function () {
            // save to local storage
            $localStorage.layout = $scope.app.layout;
        }, true);

        //global function to scroll page up
        $scope.toTheTop = function () {
            $document.scrollTopAnimated(0, 600);
        };

        // angular translate
        // ----------------------

        $scope.language = {
            // Handles language dropdown
            listIsOpen: false,
            // list of available languages
            available: {
                'en': 'English',
                'it_IT': 'Italiano',
                'de_DE': 'Deutsch'
            },
            // display always the current ui language
            init: function () {
                var proposedLanguage = $translate.proposedLanguage() || $translate.use();
                var preferredLanguage = $translate.preferredLanguage();
                // we know we have set a preferred one in app.config
                $scope.language.selected = $scope.language.available[(proposedLanguage || preferredLanguage)];
            },
            set: function (localeId, ev) {
                $translate.use(localeId);
                $scope.language.selected = $scope.language.available[localeId];
                $scope.language.listIsOpen = !$scope.language.listIsOpen;
            }
        };
        $scope.language.init();

        // Function that find the exact height and width of the viewport in a cross-browser way
        var viewport = function () {
            var e = window, a = 'inner';
            if (!('innerWidth' in window)) {
                a = 'client';
                e = document.documentElement || document.body;
            }
            return {
                width: e[a + 'Width'],
                height: e[a + 'Height']
            };
        };
        // function that adds information in a scope of the height and width of the page
        $scope.getWindowDimensions = function () {
            return {
                'h': viewport().height,
                'w': viewport().width
            };
        };
        // Detect when window is resized and set some variables
        $scope.$watch($scope.getWindowDimensions, function (newValue, oldValue) {
            $scope.windowHeight = newValue.h;
            $scope.windowWidth = newValue.w;

            if (newValue.w >= 992) {
                $scope.isLargeDevice = true;
            } else {
                $scope.isLargeDevice = false;
            }
            if (newValue.w < 992) {
                $scope.isSmallDevice = true;
            } else {
                $scope.isSmallDevice = false;
            }
            if (newValue.w <= 768) {
                $scope.isMobileDevice = true;
            } else {
                $scope.isMobileDevice = false;
            }
        }, true);
        // Apply on resize
        $win.on('resize', function () {
            $scope.$apply();
            if ($scope.isLargeDevice) {
                $('#app .main-content').css({
                    position: 'relative',
                    top: 'auto',
                    width: 'auto'
                });
                $('footer').show();
            }
        });
    }]);
