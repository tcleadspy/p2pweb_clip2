﻿declare var _: any

class KeywordsCtrl {

    private eventsArray = []
    private keywords = []
    private newKeyword = ''

    public static $inject = ["$rootScope", "$scope", "SweetAlert", "webapiSvc", "appStateSvc"];
    constructor(private $rootScope: any, private $scope: any, private SweetAlert: any, private webapiSvc: webapiSvc, private appStateSvc) {
        this.keywords = appStateSvc["keywords"]
    }

    removeAll() {
        const that = this

        this.SweetAlert.swal(KeywordsCtrl.alerts.removeAll, function (isConfirm) {
            if (isConfirm) {
                that.keywords.forEach((keyword) => { that.removeKeyword(keyword) })
            }
        })
    }

    removeKeyword(keyword) {
        const webapiSvc = this.webapiSvc
        const keywords = this.keywords
        const $scope = this.$scope

        webapiSvc.removeKeyword(keyword, function () {
            $scope.$apply(function () {
                const idx2delete = keywords.indexOf(keyword)
                keywords.splice(idx2delete, 1)
            })
        })
    }

    addKeyword() {
        const $scope = this.$scope
        const trimmedKeyword = this.newKeyword.trim().toLowerCase();
        const that = this

        if (trimmedKeyword == "")
            return

        if (this.keywords.indexOf(trimmedKeyword) != -1) {
            this.SweetAlert.swal(KeywordsCtrl.alerts.addKeyword);
        } else {
            this.webapiSvc.addKeyword(trimmedKeyword, () => {
                $scope.$apply(() => {
                    that.keywords.push(trimmedKeyword)
                    that.newKeyword = ''
                })
            })
        }
    }

    static readonly alerts = {
        addKeyword: {
            title: "Keyword already exists",
            text: "Keyword already exists",
            type: "error",
            confirmButtonColor: "#007AFF"
        }, removeAll: {
            title: "Are you sure?",
            text: "All keywords will be removed!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ok",
            cancelButtonText: "Cancel",
            closeOnConfirm: true,
            closeOnCancel: true
        }
    }

}
