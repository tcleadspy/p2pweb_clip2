'use strict';

//class P2pUserSettingsCtrl { }

/**
  * controller for User Profile Example
*/
//app.controller('P2pUserSettingsCtrl', ["$scope", "$rootScope", "flowFactory", "webapiSvc", "$timeout", "appStateSvc", function ($scope, $rootScope, flowFactory, webapiSvc, $timeout, appStateSvc) {

class P2pUserSettingsCtrl {
    public static inject = ["$scope", "$rootScope", "flowFactory", "webapiSvc", "$timeout", "appStateSvc"]

    constructor($scope, $rootScope, flowFactory, webapiSvc, $timeout, appStateSvc) {

        $scope.removeImage = function () {
            $scope.noImage = true;
        };
        //$scope.obj = new Flow();
        $scope.dataObj = appStateSvc

        $scope.$watch('dataObj.fbincorporated', function (newValue, oldValue) {
            if (newValue == oldValue) return

            console.log('change detected. new=' + newValue + ' old=' + oldValue)
            webapiSvc.updateUserOptions({ fbincorporated: newValue, url: appStateSvc.url4msgs }, function () {
                console.log('change saved to server')
                window.location.reload()
            })
        })

        $scope.getUserOptionsObject = function (dataObj) {
            return { fbincorporated: dataObj.fbincorporated, url4msgs: appStateSvc.url4msgs }
        }

        $scope.timeoutfade = function () {
            $timeout(function () {
                $scope.statushidden = false
                $scope.statusfade = true
            }, 1000)
        }

        $scope.ldloading = {};
        $scope.statusmsg = ''
        $scope.statusfade = false
        $scope.statushidden = false
        $scope.saving = true

        $scope.save = function () {
            $scope.saving = false
            $scope.ldloading['zoom_in'] = true
            webapiSvc.updateUserOptions($scope.getUserOptionsObject(appStateSvc), function () {
                $scope.ldloading['zoom_in'] = false
                $scope.statusmsg = 'save completed'
                $scope.timeoutfade()
                console.log('change saved to server')
                $scope.$apply(() => { $scope.saving = false })
            }, function () {
                $scope.ldloading['zoom_in'] = false
                $scope.statusmsg = 'save failed'
                $scope.timeoutfade()
                console.log('error saving user options')
                $scope.url4msgs = appStateSvc.url4msgs
                $scope.$apply(() => { $scope.saving = false })
            })
        }

        $scope.fbincorporatedClick = function () {
            console.log('arguments.length = ' + arguments.length)
        }

        $scope.userInfo = {
            firstName: $rootScope.user.name, // 'Peter',
            //lastName: 'Clark',
            url: appStateSvc.url4msgs, // 'www.example.com',
            //email: 'peter@example.com',
            //phone: '(641)-734-4763',
            //gender: 'male',
            //zipCode: '12345',
            //city: 'London (UK)',
            //avatar: '/Scripts/assets/images/avatar-1-xl.jpg',
            //twitter: '',
            //github: '',
            facebook: $rootScope.user.name,
            //linkedin: '',
            //google: '',
            //skype: 'peterclark82'
        };
        if ($scope.userInfo.avatar == '') {
            $scope.noImage = true;
        }
    }

}

