'use strict';
/**
 * Clip-Two Main Controller
 */
app.controller('P2pAppCtrl', ['$rootScope', '$scope', '$state', '$translate', '$localStorage', '$window', '$document', '$timeout', 'cfpLoadingBar', '$transitions', 'cacheSvc', 'fbookSvc', 'common', 'appStateSvc', 'cacheMgrSvc',
    function ($rootScope, $scope, $state, $translate, $localStorage, $window, $document, $timeout, cfpLoadingBar, $transitions, cacheSvc, fbookSvc, common, appStateSvc, cacheMgrSvc) {

        // Loading bar transition
        // -----------------------------------
        const $win = $($window);

        $scope.loaded = false
        $scope.maxCacheAgeInHrs = 24
        $scope.percentFinished = 10
        $scope.statusmsg = "Initializing"
        $rootScope.pageSize = 10

        $scope.updateStatus = function (msg) {
            $scope.statusmsg = msg
            common.safeApply($scope)
        }

        $scope.navElements = [
            { sref: "app.facebookfeeds", icon: "ti-facebook", fa_icon: "fa-facebook", titleTranslate: "sidebar.nav.element.FACEBOOK", defaultText: "Feeds", desc: 'Add or remove Facebook feeds' },
            { sref: "app.keywords", icon: "ti-key", fa_icon: "fa-key", titleTranslate: "sidebar.nav.element.KEYWORDS", defaultText: "Keywords", desc: 'Add or remove default keywords' },
            { sref: "app.mining", icon: "ti-search", fa_icon: "fa-search", titleTranslate: "sidebar.nav.element.MINING", defaultText: "Mine", desc: 'Mine the data, find matching comments, contact prospects' },
            { sref: "app.leadspyview", icon: "ti-desktop", fa_icon: "fa-desktop", titleTranslate: "sidebar.nav.element.RETRO", defaultText: "Retro View", desc: 'Choose feeds to mine, find matching comments, contact prospects' },
            { sref: "app.starred", icon: "ti-star", fa_icon: "fa-star", titleTranslate: "sidebar.nav.element.STARRED", defaultText: "Starred Comments", desc: 'Review your starred comments' },
        ]

        $rootScope.updatePageSize = function (newPageSize) {
            $rootScope.pageSize = newPageSize
        }
        $rootScope.logEvent = function (rootscope, scope) {
            rootscope.updatePageSize(scope.tableParams.count())
        }

        $scope.appStateSvc = appStateSvc

        /* update 'loaded' flag */
        $scope.setReady = function (loaded) {
            $scope.$apply(function () { $scope.loaded = loaded })
        }

        /* take the initializer passed from the view via a window property */

        /* open indexeddb (async) */
        $scope.statusmsg = "opening feed cache.."
        const results = $window.p2pinitializer
        $rootScope.user.name = results["username"]
        appStateSvc.applyResults(results)
        cacheMgrSvc.initialize($scope, $scope.maxCacheAgeInHrs)

        $transitions.onStart({}, function (trans) {
            cfpLoadingBar.start();
            if (typeof CKEDITOR !== 'undefined') {
                for (name in CKEDITOR.instances) {
                    CKEDITOR.instances[name].destroy();
                }
            }
        });

        $transitions.onSuccess({}, function (trans) {
            //stop loading bar on stateChangeSuccess
            $scope.$on('$viewContentLoaded', function (event) {
                cfpLoadingBar.complete();
            });

            // scroll top the page on change state
            $('#app .main-content').css({
                position: 'relative',
                top: 'auto'
            });

            $('footer').show();

            window.scrollTo(0, 0);

            if (angular.element('.email-reader').length) {
                angular.element('.email-reader').animate({
                    scrollTop: 0
                }, 0);
            }

            // Save the route title
            $rootScope.currTitle = $state.current.title;
        });

        $rootScope.pageTitle = function () {
            return $rootScope.app.name + ' - ' + ($rootScope.currTitle || $rootScope.app.description);
        };

        // save settings to local storage
        if (angular.isDefined($localStorage.layout)) {
            $scope.app.layout = $localStorage.layout;

        } else {
            $localStorage.layout = $scope.app.layout;
        }
        $scope.$watch('app.layout', function () {
            // save to local storage
            $localStorage.layout = $scope.app.layout;
        }, true);

        //global function to scroll page up
        $scope.toTheTop = function () {
            $document.scrollTopAnimated(0, 600);
        };

        // angular translate
        // ----------------------

        $scope.language = {
            // Handles language dropdown
            listIsOpen: false,
            // list of available languages
            available: {
                'en': 'English',
                'it_IT': 'Italiano',
                'de_DE': 'Deutsch'
            },
            // display always the current ui language
            init: function () {
                var proposedLanguage = $translate.proposedLanguage() || $translate.use();
                var preferredLanguage = $translate.preferredLanguage();
                // we know we have set a preferred one in app.config
                $scope.language.selected = $scope.language.available[(proposedLanguage || preferredLanguage)];
            },
            set: function (localeId, ev) {
                $translate.use(localeId);
                $scope.language.selected = $scope.language.available[localeId];
                $scope.language.listIsOpen = !$scope.language.listIsOpen;
            }
        };
        $scope.language.init();

        // Function that find the exact height and width of the viewport in a cross-browser way
        var viewport = function () {
            var e = window, a = 'inner';
            if (!('innerWidth' in window)) {
                a = 'client';
                e = document.documentElement || document.body;
            }
            return {
                width: e[a + 'Width'],
                height: e[a + 'Height']
            };
        };
        // function that adds information in a scope of the height and width of the page
        $scope.getWindowDimensions = function () {
            return {
                'h': viewport().height,
                'w': viewport().width
            };
        };
        // Detect when window is resized and set some variables
        $scope.$watch($scope.getWindowDimensions, function (newValue, oldValue) {
            $scope.windowHeight = newValue.h;
            $scope.windowWidth = newValue.w;

            if (newValue.w >= 992) {
                $scope.isLargeDevice = true;
            } else {
                $scope.isLargeDevice = false;
            }
            if (newValue.w < 992) {
                $scope.isSmallDevice = true;
            } else {
                $scope.isSmallDevice = false;
            }
            if (newValue.w <= 768) {
                $scope.isMobileDevice = true;
            } else {
                $scope.isMobileDevice = false;
            }
        }, true);
        // Apply on resize
        $win.on('resize', function () {
            $scope.$apply();
            if ($scope.isLargeDevice) {
                $('#app .main-content').css({
                    position: 'relative',
                    top: 'auto',
                    width: 'auto'
                });
                $('footer').show();
            }
        });
    }]);
