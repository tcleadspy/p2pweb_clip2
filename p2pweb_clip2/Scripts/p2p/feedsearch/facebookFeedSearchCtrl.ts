﻿'use strict';

class FacebookFeedSearchCtrl {

    searchTerm: string = ""
    lastSearchedTerm: string = ""
    feedsMatchingSearch = []
    displayedFeeds = []
    selectedFeedType = 'pages'
    feedTypes = ['pages', 'groups']
    searchInProgress = false

    static $inject = ["$scope", "$uibModal", "$log", 'SweetAlert', '$rootScope', "webapiSvc", "appStateSvc", "cacheSvc", "fbookSvc", "common"]
    constructor(
        private $scope, private $uibModal, private $log, private SweetAlert,
        private $rootScope, private webapiSvc,
        private appStateSvc, private cacheSvc, private fbookSvc, private common) {

        const that = this
        $scope.$watch('vm.selectedFeedType', () => {
            that.feedsMatchingSearch.length = 0
            that.displayedFeeds.length = 0
            that.lastSearchedTerm = ''
            if (!$scope.$$phase)
                $scope.$apply()
        })
    }


    private showResults(results, sortFun) {
        const $scope = this.$scope
        const feeds = this.appStateSvc.feedsSelected
        const that = this
        const common = that.common

        that.lastSearchedTerm = that.searchTerm

        results.forEach((fbFeed) => {
            fbFeed.alreadySelected = (common.searchArrayForID(feeds, fbFeed.id) !== -1)
            fbFeed.updated_time = fbFeed.updated_time || 0
            fbFeed.i_fan_count = fbFeed.fan_count ? parseInt(fbFeed.fan_count) : 0
            fbFeed.ldloading = {}
        })

        /* make order descending via .reverse() */
        results.sort(sortFun)
        results.reverse()
        that.feedsMatchingSearch = results
        that.displayedFeeds = that.feedsMatchingSearch.slice(0, 50)
        that.searchInProgress = false
        $scope.$apply()
    }


    /* initiate a search operation */
    startSearch() {
        if (!this.searchTerm)
            return

        this.searchInProgress = true
        switch (this.selectedFeedType) {
            case 'groups': this.doGroupSearch(); break
            case 'pages': this.doPagesSearch(); break
        }
    }
    doGroupSearch() {
        const that = this
        that.fbookSvc.searchForGroups(that.searchTerm, (results) => {
            if (results.length) {
                that.showResults(results, (f1, f2) => { return f1.updated_time - f2.updated_time })
            } else if (that.looksLikeFBID(that.searchTerm)) {
                that.selectByFeedID()
            }
        })
    }
    doPagesSearch() {
        const that = this
        that.fbookSvc.searchForPages(that.searchTerm, (results) => {
            if (results.length) {
                that.showResults(results, (f1, f2) => { return f1.i_fan_count - f2.i_fan_count })
            } else if (that.looksLikeFBID(that.searchTerm)) {
                that.selectByFeedID()
            }
        })
    }

    /* check for a string of characters that are probably an FB ID */
    looksLikeFBID(str) {
        try {
            parseInt(str)
            return true
        } catch (ex) {
            return false
        }
    }

    /* load additional results */
    loadmore() {
        if (this.displayedFeeds.length >= this.feedsMatchingSearch.length)
            return

        const displayedLength = this.displayedFeeds.length
        const feedsMatchingLength = this.feedsMatchingSearch.length
        const lenDiff = feedsMatchingLength - displayedLength
        const endidx = displayedLength + (lenDiff > 50 ? 50 : lenDiff)

        this.feedsMatchingSearch.slice(displayedLength, endidx).forEach((feed) => {
            this.displayedFeeds.push(feed)
        })
    }


    /*
     * add a feed for monitoring.
     * adds the feed to the database
     * downloads the feed contents from facebook
     * saves feed contents to local cache
     */
    addFeed(feed) {
        const $rootScope = this.$rootScope
        const $scope = this.$scope
        const feedsSelected = this.appStateSvc["feedsSelected"]
        const fbooksvc = this.fbookSvc
        const cache = this.cacheSvc

        feed.type = (this.selectedFeedType == 'groups') ? 'group' : 'page'
        feed.feedType = feed.type
        feed.ldloading['zoom_in'] = true
        feed.busy = true

        this.webapiSvc.addPageWatch(feed,
            (results) => {
                $scope.$apply(() => {
                    feed.ldloading['zoom_in'] = false
                    feed.busy = false
                    feed.alreadySelected = true
                    feedsSelected.push(feed)
                })

                fbooksvc.getFeed(feed.id,
                    (data) => {
                        if (data !== null) {
                            cache.saveFeed(new feedCacheEntry(feed, data))
                            $rootScope.$apply(() => { feed.timestamp = new Date() })
                        }
                    },
                    (err) => { this.$log.error(err) })
            })
    }

    /*
     * stop monitoring a given feed
     * removes the feed from the server database for this user
     * removes the feed from the browser cache
     */
    removeFeed(feed) {
        const $scope = this.$scope
        const feedsSelected = this.appStateSvc["feedsSelected"]
        const cache = this.cacheSvc
        const common = this.common

        feed.busy = true
        feed.ldloading['zoom_in'] = true

        this.webapiSvc.removePageWatch(feed.id, (results) => {
            feed.busy = false
            cache.removeFeed(feed.id)

            const idx2Delete = common.searchArrayForID(feedsSelected, feed.id)
            if (idx2Delete === -1) return
            $scope.$apply(() => {
                feedsSelected.splice(idx2Delete, 1)
                feed.alreadySelected = false
                feed.ldloading['zoom_in'] = false
            })
        })
    }

    /*
     * add a feed for monitoring.
     * adds the feed to the database
     * downloads the feed contents from facebook
     * saves feed contents to local cache
     */
    addGroupFeed(feed) {
        const $rootScope = this.$rootScope
        const $scope = this.$scope
        const feedsSelected = this.appStateSvc["feedsSelected"]
        const fbooksvc = this.fbookSvc
        const cache = this.cacheSvc
        const that = this

        this.updateBusyState(feed, true)

        fbooksvc.getFeed(feed.id,
            (data) => {
                if (!data || data.error) {
                    that.$log.error("invalid data returned: " + JSON.stringify(data))
                    feed.errmsg = "invalid data returned: " + JSON.stringify(data)
                    that.updateBusyState(feed, false)
                } else {
                    const newobj = new feedCacheEntry(feed, data)
                    newobj.feedType = 'group'
                    cache.saveFeed(newobj)
                    $rootScope.$apply(() => { feed.timestamp = new Date() })

                    that.webapiSvc.addGroupWatch(feed, (results) => {
                        that.updateBusyState(feed, false)
                        $scope.$apply(() => {
                            feed.alreadySelected = true
                            feedsSelected.push(feed)
                        })
                    })
                }
            },
            (err) => {
                feed.errmsg = "error ocurred: " + err
                that.$log.error(err)
                that.updateBusyState(feed, false)
            })

    }

    /*
     * update the busy state of the selected feed to 'loading'
     */
    updateBusyState(feed, loading) {
        feed.busy = loading
        feed.ldloading['zoom_in'] = loading
    }

    /*
     * stop monitoring a given feed
     * removes the feed from the server database for this user
     * removes the feed from the browser cache
     */
    removeGroupFeed(feed) {
        const $scope = this.$scope
        const feedsSelected = this.appStateSvc["feedsSelected"]
        const cache = this.cacheSvc
        const common = this.common

        const idx2Delete = common.searchArrayForID(feedsSelected, feed.id)
        if (idx2Delete === -1)
            return

        this.updateBusyState(feed, true)

        this.webapiSvc.removePageWatch(feed.id, (results) => {
            this.updateBusyState(feed, false)
            cache.removeFeed(feed.id)
            $scope.$apply(() => {
                feedsSelected.splice(idx2Delete, 1)
                feed.alreadySelected = false
            })
        })
    }


    /*
     * choose a feed to watch directly by id rather than searching
     */
    selectByFeedID() {
        const that = this
        const typestr = this.selectedFeedType.substring(0, this.selectedFeedType.length - 1)
        const inputValue = this.searchTerm
        const errmsg = "An error has occurred. " + typestr + " " + inputValue + " not found."
        const errobj = { title: "Ruh roh", text: errmsg, confirmButtonColor: "#007AFF" }

        that.searchInProgress = true
        if (that.selectedFeedType === 'groups') {
            that.fbookSvc.getGroupInfo(inputValue, (group) => {
                that.searchInProgress = false
                if (!group.error) {
                    that.showResults([group], (f1, f2) => { return f1.updated_time - f2.updated_time })
                } else {
                    that.SweetAlert.swal(errobj);
                }
            })
        } else if (that.selectedFeedType === 'pages') {
            that.fbookSvc.getPageInfo(inputValue, (page) => {
                that.searchInProgress = false
                if (!page.error) {
                    that.showResults([page], (f1, f2) => { return f1.i_fan_count - f2.i_fan_count })
                } else {
                    that.SweetAlert.swal(errobj);
                }
            })
        }
    }


}

