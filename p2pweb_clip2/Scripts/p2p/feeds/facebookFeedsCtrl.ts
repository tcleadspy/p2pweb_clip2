﻿declare var _: any

class FacebookFeedsCtrl {

    public static $inject = ["$rootScope","$scope","SweetAlert","$log","fbookSvc","common","appStateSvc","cacheSvc","webapiSvc"];
    constructor(private $rootScope, private $scope: any, private SweetAlert, private $log, private fbookSvc, private common, private appStateSvc, private cacheSvc, private webapiSvc) {
        this.pages = this.appStateSvc.feedsSelected.filter((feed) => { return feed.feedType === 'page' || feed.type === 'page' })
        this.groups = this.appStateSvc.feedsSelected.filter((feed) => { return feed.feedType === 'group' || feed.type === 'group' })
    }

    pages = []
    groups = []
    refreshingAll = false

    /*
    * re-download all feeds into cache
    */
    refreshAll() {
        const feeds = this.appStateSvc.feedsSelected
        const $scope = this.$scope
        const that = this

        let num = feeds.length
        let feedcomplete = (feed) => {
            if (!--num) {
                that.refreshingAll = false
                if (!$scope.$$phase)
                    $scope.$apply()
            }
        }

        this.refreshingAll = true
        this.appStateSvc.feedsSelected.forEach((feed) => {
            this.refreshFeed(feed, feedcomplete)
        })
    }

    /*
    * re-download the feed contents
    * update the cache entry
    */
    refreshFeed(feed, done) {
        const $scope = this.$scope
        const $rootScope = this.$rootScope
        const cache = this.cacheSvc

        feed.refreshing = true
        this.fbookSvc.getFeed(feed.id,
            (data) => {
                $scope.$apply(() => { feed.refreshing = false })
                if (data !== null) {
                    cache.saveFeedWithCallback(new feedCacheEntry(feed, data), () => {
                        $rootScope.$apply(() => { feed.timestamp = new Date() })
                        if (done) done()
                    })
                    //cache.removeFeedWithCallback(feed.id, () => {
                    //})
                }
            }, () => {
                feed.refreshing = false
                const alertObj = (typeof (FB) === "undefined") ? FacebookFeedsCtrl.alerts.fbdisabled : FacebookFeedsCtrl.alerts.refresherror
                this.SweetAlert.swal(alertObj);
                if (done) done()
            })
    }

    /*
     * remove feed from cache and in-memory buffer
     * refresh this.pages and this.groups
     */
    removeFeedCore(feed) {
        const common = this.common
        const cache = this.cacheSvc
        const feedsSelected = this.appStateSvc["feedsSelected"]
        const $scope = this.$scope

        cache.removeFeed(feed.id)

        const idx2Delete = common.searchArrayForID(feedsSelected, feed.id)
        if (idx2Delete === -1)
            return

        feedsSelected.splice(idx2Delete, 1)
        feed.alreadySelected = false
        this.pages = this.appStateSvc.feedsSelected.filter((feed) => { return feed.feedType === 'page' || feed.type === 'page' })
        this.groups = this.appStateSvc.feedsSelected.filter((feed) => { return feed.feedType === 'group' || feed.type === 'group' })

        $scope.$apply()
    }

    /*
     * stop monitoring a given feed
     * removes the feed from the server database for this user
     */
    removeFeed(feed) {
        const common = this.common
        const idx2Delete = common.searchArrayForID(this.appStateSvc["feedsSelected"], feed.id)
        if (idx2Delete === -1) return

        /* trigger the icon to spin during removal */
        feed.busy = true

        const that = this

        if (feed.feedType === 'page' || feed.type === 'page') {
            this.webapiSvc.removePageWatch(feed.id, (results) => {
                that.removeFeedCore(feed)
            })
        } else if (feed.feedType === 'group' || feed.type === 'group') {
            this.webapiSvc.removeGroupWatch(feed.id, (results) => {
                that.removeFeedCore(feed)
            })
        }
    }

    /*
     * remove all feeds from watch list and cache
     */
    removeAll() {
        const feedsSelected = this.appStateSvc.feedsSelected
        const that = this

        this.SweetAlert.swal(FacebookFeedsCtrl.alerts.removall, function (isConfirm) {
            if (isConfirm) {
                feedsSelected.forEach((feed) => {
                    that.removeFeed(feed)
                })
            }
        });
    }


    /*
     * SweetAlert alert objects
     */
    static readonly alerts = {
        fbdisabled: {
            title: "Facebook access disabled",
            text: "Facebook access is currently disabled. Change the setting to re-enable facebook updates.",
            type: "error",
            confirmButtonColor: "#007AFF"
        }, refresherror: {
            title: "An error occurred",
            text: "Error occurred, refresh did not complete successfully.",
            type: "error",
            confirmButtonColor: "#007AFF"
        }, removall: {
            title: "Are you sure?",
            text: "All feeds will be removed!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ok",
            cancelButtonText: "Cancel",
            closeOnConfirm: true,
            closeOnCancel: true
        }
    }


}

