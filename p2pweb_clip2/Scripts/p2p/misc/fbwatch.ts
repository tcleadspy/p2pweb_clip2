﻿class fbwatch {
    constructor(
        private feedID: string,
        private name: string
    ){}

}

class feedCacheEntry {
    public id: string
    public name: string
    public timestamp: string
    public data: any
    public feedType: string = "page"

    public constructor(feed: any, data: any) {
        this.id = feed.id
        this.name = feed.name
        this.data = data
        this.timestamp = new Date().toString()
    }
}

class groupCacheEntry {
    public id: string
    public name: string
    public timestamp: string
    public data: any
    public feedType: string = "group"

    public constructor(feed: any, data: any) {
        this.id = feed.id
        this.name = feed.name
        this.data = data
        this.timestamp = new Date().toString()
    }
}
