﻿///// <reference path="../typings/knockout/knockout.d.ts" />

declare var FB: any;

class fbook {
    //constructor(
    //    private app_id: string,
    //    private access_token: string,
    //)
    //{ }

    constructor() {

    }

    private static qryFeedSmallOld: string = "/feed?fields=message,comments{like_count,from,id,message,created_time,is_hidden,comment_count},likes.limit(1).summary(true),updated_time&limit=50";
    private static qryFeedSmall2: string = "/feed?fields=message,comments{from, comments ,like_count,from,id,message,created_time,is_hidden,comment_count},likes.limit(1).summary(true),updated_time&limit=50";
    private static qryFeedSmall: string = "/feed?fields=message,comments{from, message, created_time, comments{from, message, created_time}},likes.limit(1).summary(true),limit=50";
    private static qryFeedMsgOnly: string = "/feed?fields=message,updated_time&limit=50";
    private static qryPageInfo: string = "?fields=name,about,category,fan_count,likes.limit(100)";
    private static qryGroupInfo: string = "?fields=name,category,fan_count,likes.limit(100)";
    private static qryPostInfo: string = "?fields=message,likes.limit(1).summary(true),comments.limit(1).summary(true)";
    private static qrySearchPages: string = "search?q={0}&fields=name,about,category,fan_count&limit=1000&type=Page";
    private static qrySearchGroups: string = "search?q={0}&fields=name,privacy&limit=1000&type=Group";

    public static numCalls: number = 0;

    public static callDetails: string [] = [];

    public fbApi(qry: string, onSuccess: any, onError: any) {
        if (typeof(FB) === "undefined") {
            if (onError)
                onError()

            return
        }
        FB.api(qry, onSuccess);

        fbook.numCalls++; //fbook.numCalls() + 1);
        fbook.callDetails.push(qry);
    }

    public getFeed(feedID: string, onSuccess: any, onError: any) {
        this.fbApi(feedID + fbook.qryFeedSmall, onSuccess, onError);
    }

    public getPostInfo(postID: string, onSuccess: any): any {
        this.fbApi(postID + fbook.qryPostInfo, onSuccess, null);
    }

    public getGroupInfo(postID: string, onSuccess: any): any {
        this.fbApi(postID + fbook.qryGroupInfo, onSuccess, null);
    }

    public searchForPages(searchTerm: string, onSuccess: any) {
        this.searchPagesOrGroups(
            fbook.qrySearchPages.replace("{0}", searchTerm),
            onSuccess);
    }

    public searchForGroups(searchTerm: string, onSuccess: any) {
        this.searchPagesOrGroups(
            fbook.qrySearchGroups.replace("{0}", searchTerm),
            onSuccess);
    }

    private searchPagesOrGroups(qry: string, onSuccess: any) {
        this.fbApi(qry, function (response) {
            const searchFeedsResults: any[] = [];

            response.data.forEach(d => {
                searchFeedsResults.push(d)
            });

            onSuccess(searchFeedsResults);
        }, null);
    }


}