﻿'use strict';

declare var app: any;
declare var angular: any;
/**
 * controller for Messages
 */
class RetroCtrl {

    static inject = ["$rootScope", "$scope", "$state", "$interval", "$log", "facebookMiner", "common", "SweetAlert", "webapiSvc", "fbookSvc", "appStateSvc", "cacheSvc"]
    constructor($rootScope, $scope, $state, $interval, $log, facebookMiner: facebookMiner, common, SweetAlert, webapiSvc, fbookSvc, appStateSvc, cacheSvc) {
        let getPane = function (paneName) {
            paneName = paneName.toLowerCase()
            return $scope.panes.filter((pane) => { return pane.header.toLowerCase() === paneName })[0]
        }
        let getFeedsToMine = function () {
            let feeds = []

            $scope.panes.forEach((pane) => {
                const selectedFeedsInPane = pane.feeds.filter((feed) => { return feed.selected })
                selectedFeedsInPane.forEach((feed) => feed.feedType = pane.feedType)
                feeds = feeds.concat(selectedFeedsInPane)
                //feeds = feeds.concat(pane.feeds.filter((feed) => { return feed.selected }))
            })

            return feeds
        }
        let getKeywords = function () {
            if (!$scope.filterOn)
                return []

            return appStateSvc.keywords
        }
        let safeapply = function () {
            if (!$scope.$$phase)
                $scope.$apply
        }

        /* select feeds to mine */
        let selectAllOnPane = function (pane) {
            pane.feeds.forEach((feed) => { feed.selected = true })
        }
        let deselectAllOnPane = function (pane) {
            pane.feeds.forEach((feed) => { feed.selected = false })
        }
        let selectAll = function () {
            selectAllOnPane(getPane('pages'))
            selectAllOnPane(getPane('groups'))
            updateSelectedStatuses()
        }

        /* object creation helper */
        let getflatObj = function (page, post, comment) {
            const numlikes = (comment.likes) ? comment.likes.summary.total_count : 0

            /* clear out sub-comments so that keyword search works w/o matching subs */
            comment.comments = []
            return {
                id: comment.id,
                date: comment.created_time,
                pageid: page.id,
                postid: post.id,
                likes: numlikes,
                commenttext: comment.message,
                comment: comment
            }
        }
        /* convert the results graph (tree) to a flat list */
        let resultstoflatlist = function (pages) {
            let result = []
            const that = this

            pages.forEach(function (page) {
                page.data.forEach(function (post) {
                    post.comments.data.forEach(function (comment) {
                        result.push(getflatObj(page, post, comment))
                    })
                })
            })

            return result
        }
        let startminecore = function (success) {
            facebookMiner.startmine(getKeywords(), (results) => {
                const feedsToMine = getFeedsToMine()
                results = results.filter((result) => { return common.searchArrayForID(feedsToMine, result.id) !== -1 })
                const flatresults = resultstoflatlist(results)
                const readComments = cacheSvc.getReadComments()
                const filteredresults = flatresults.filter((c) => { return readComments.indexOf(c.id) !== -1 })
                filteredresults.forEach((c) => { c.read = true })
                const starredComments = cacheSvc.getStarredComments()
                starredComments.forEach((commentID) => {
                    let comment = null
                    for (let i = 0; i < flatresults.length; i++) {
                        if (flatresults[i].id === commentID) {
                            flatresults[i].starred = true
                        }
                    }
                })
                $scope.messages = flatresults
                $log.log('mine complete')
                if (success)
                    success()
            })
        }
        let mineandapply = function () {
            startminecore(() => {
                safeapply()
            })
        }
        let isPaneFullySelected = function (pane) {
            const paneLength = pane.feeds.length
            const selectedPageCount = pane.feeds.filter((feed) => { return feed.selected === true }).length

            if (selectedPageCount == 0)
                return feedPaneSelectionStates.none
            else if (selectedPageCount < paneLength)
                return feedPaneSelectionStates.partial
            else if (selectedPageCount === paneLength)
                return feedPaneSelectionStates.all
            else
                throw ('selectedFeedCount: ' + selectedPageCount + " pane.feeds.length: " + paneLength)
        }
        let updateSelectedStatuses = function () {
            const pagesPane = getPane('pages')
            const groupsPane = getPane('groups')

            pagesPane.state = isPaneFullySelected(pagesPane)
            groupsPane.state = isPaneFullySelected(groupsPane)
        }

        enum feedPaneSelectionStates { none, partial, all }
        $scope.toggleFeedSelected = function (feed) {
            feed.selected = !feed.selected
            updateSelectedStatuses()
        }
        $scope.panes = [{ header: 'Pages', feedType: 'page', feeds: [], state: feedPaneSelectionStates.none }, { header: 'Groups', feedType: 'group', feeds: [], state: feedPaneSelectionStates.none }]
        $scope.filterOn = true
        appStateSvc.feedsSelected.forEach((feed) => {
            const paneIdx = (feed.feedType == 'page' ? 0 : 1)
            $scope.panes[paneIdx].feeds.push({ name: feed.name, id: feed.id })
        })
        $scope.matchingComments = []
        $scope.startmine = function () {
            startminecore(() => {
                safeapply()
            })
        }
        $scope.sendmessage = function (targetid) {
            let _this = this;
            fbookSvc.send(targetid,
                () => { $log.log('success') },
                () => { $log.log('error on the sent messages save') }
            );
        }
        $scope.noAvatarImg = "/Scripts/assets/images/default-user.png";
        $scope.messages = []
        const alerts = {
            deleteSelected: {
                title: "Are you sure?", text: "Selected feeds will be removed!", type: "warning", showCancelButton: true, confirmButtonColor: "#DD6B55",
                confirmButtonText: "Ok",cancelButtonText: "Cancel",closeOnConfirm: true,closeOnCancel: true
            },
            /* error detail objects */
            swaNoFB: { title: "Facebook access disabled", text: "Facebook access is currently disabled. Change the setting to re-enable facebook updates.", type: "error", confirmButtonColor: "#007AFF" },
            swaErr: { title: "An error occurred", text: "Error occurred, refresh did not complete successfully.", type: "error", confirmButtonColor: "#007AFF" }
        }
        $scope.deleteSelected = function () {
            const feedsToMine = getFeedsToMine()

            SweetAlert.swal(alerts.deleteSelected, function (isConfirm) {
                if (isConfirm) {
                    let cleanupFun = (feedID, results) => {
                        cacheSvc.removeFeed(feedID)
                        $scope.panes.forEach((pane) => {
                            pane.feeds = pane.feeds.filter((feed) => { return feed.id !== feedID })
                        })
                        appStateSvc.feedsSelected = appStateSvc.feedsSelected.filter((feed4) => { return feed4.id !== feedID })
                        $scope.$apply()
                        return results
                    }
                    feedsToMine.forEach((feed) => {
                        if (feed.feedType === 'page')
                            webapiSvc.removePageWatch(feed.id, (results) => { cleanupFun(feed.id, results) })
                        else
                            webapiSvc.removeGroupWatch(feed.id, (results) => { cleanupFun(feed.id, results) })
                    })
                }
            });

        }
        $scope.selectAllSingleType = function (paneName) {
            selectAllOnPane(getPane(paneName))
            updateSelectedStatuses()
        }
        $scope.deselectAllSingleType = function (paneName) {
            deselectAllOnPane(getPane(paneName))
            updateSelectedStatuses()
        }
        $scope.$watch('filterOn', () => {
            startminecore(() => {
                if (!$scope.$$phase)
                    $scope.$apply()
            })
        })
        let selectedMessage = null
        $scope.selectMessage = function (message) {
            /* when new one is selected, de-selected previously selected */
            if (selectedMessage) selectedMessage.selected = false
            message.selected = true
            selectedMessage = message

            if (!message.read) {
                message.read = true
                cacheSvc.addReadComment(message.id, null)
            }
        }



        /* re-download the feed contents, update the cache entry */
        let refreshFeed = function (feed, done) {
            feed.refreshing = true
            fbookSvc.getFeed(feed.id,
                (data) => {
                    $scope.$apply(() => { feed.refreshing = false })
                    if (data !== null) {
                        cacheSvc.removeFeedWithCallback(feed.id, () => {
                            cacheSvc.saveFeedWithCallback(new feedCacheEntry(feed, data), () => {
                                $rootScope.$apply(() => { feed.timestamp = new Date() })
                                if (done) done()
                            })
                        })
                    } else {
                        if (done) done()
                    }
                }, () => {
                    feed.refreshing = false
                    if (typeof (FB) === "undefined")
                        this.SweetAlert.swal(alerts.swaNoFB)
                    else
                        this.SweetAlert.swal(alerts.swaErr)
                    //also works setTimeout(function () { $scope.$apply(function () { feed.refreshing = false }) }, 3000)
                    if (done) done()
                })
        }
        $scope.refresh = function () {
            const feeds = getFeedsToMine()
            let num = feeds.length
            let feedcomplete = (feed) => {
                if (!--num)
                    mineandapply()
            }
            feeds.forEach((feed) => { refreshFeed(feed, feedcomplete) })
        }
        $scope.autorefresh = false
        let autorefresh_timer = null
        $scope.$watch('autorefresh', () => {
            if (autorefresh_timer) {
                $log.log('clearing auto-refresh timer')
                clearInterval(autorefresh_timer)
            }
            if ($scope.autorefresh) {
                $log.log('starting auto-refresh at 1 minute intervals')
                autorefresh_timer = setInterval(() => { $scope.refresh() }, 60000)
            }
        })
        $scope.lookup = function (key) {
            const matches = appStateSvc.feedsSelected.filter((f) => { return f.id === key })
            if (matches.length !== 1)
                throw ('lookup failed for key: ' + key + " matches found: " + matches)
            const match = matches[0]
            return match.name
        }
        $scope.starmessage = function (message) {
            if (!message.starred) {
                cacheSvc.addSavedComment(message, () => {
                    message.starred = !message.starred
                    safeapply()
                })
            } else {
                cacheSvc.removeSavedComment(message.id, () => {
                    message.starred = !message.starred
                    safeapply()
                })
            }
        }

        /* run an initial mine operation on all available feeds (perhaps should maintain active set) */
        selectAll()
        $scope.startmine()
    }
}


class CommentDetailsCtrl {
    static inject = ['$scope', '$stateParams', 'common']

    constructor($scope, $stateParams, common) {
        $scope.message = this.getById($scope.messages, $stateParams.msgid);
        //$scope.message = common.searchArrayForID($scope.messages, $stateParams.msgid);
    }

    getById(arr, id) {
        for (var d = 0, len = arr.length; d < len; d += 1) {
            if (arr[d].id == id) {
                return arr[d];
            }
        }
    }

}

//        $scope.scopeVariable = 1;
//        var loadMessage = function () {
//            $scope.messages.push(incomingMessages[$scope.scopeVariable - 1]);
//            $scope.scopeVariable++;
//        };
//        //Put in interval, first trigger after 10 seconds
//        var add = $interval(function () {
//            if ($scope.scopeVariable < 4) {
//                loadMessage();
//            }
//        }, 10000);
//        $scope.stopAdd = function () {
//            if (angular.isDefined(add)) {
//                $interval.cancel(add);
//                add = undefined;
//            }
//        };
//    }

