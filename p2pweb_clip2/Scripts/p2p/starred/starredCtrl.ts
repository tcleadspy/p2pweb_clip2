﻿'use strict';

declare var app: any;
declare var angular: any;
/**
 * controller for Messages
 */
class StarredCtrl {

    static inject = ["$scope", "$state", "$interval", "$log", "facebookMiner", "common", "SweetAlert", "webapiSvc", "fbookSvc", "appStateSvc", "cacheSvc"]
    constructor($scope, $state, $interval, $log, facebookMiner: facebookMiner, common, SweetAlert, webapiSvc, fbookSvc, appStateSvc, cacheSvc) {
        let safeapply = function () {
            if (!$scope.$$phase)
                $scope.$apply()
        }
        $scope.matchingComments = []
        $scope.sendmessage = function (targetid) {
            let _this = this;
            fbookSvc.send(targetid,
                () => { $log.log('success') },
                () => { $log.log('error on the sent messages save') }
            );
        }
        $scope.noAvatarImg = "/Scripts/assets/images/default-user.png";
        $scope.messages = []
        let selectedMessage = null
        $scope.selectMessage = function (message) {
            /* when new one is selected, de-selected previously selected */
            if (selectedMessage) selectedMessage.selected = false
            message.selected = true
            selectedMessage = message

            if (!message.read) {
                message.read = true
                cacheSvc.addReadComment(message.id, null)
            }
        }

        $scope.lookup = function (key) {
            const matches = appStateSvc.feedsSelected.filter((f) => { return f.id === key })
            if (matches.length !== 1) {
                //throw ('lookup failed for key: ' + key + " matches found: " + matches)
                return key
            }
            const match = matches[0]
            return match.name
        }
        $scope.starmessage = function (message) {
            cacheSvc.removeSavedComment(message.id, () => {
                message.starred = false
                $scope.messages = $scope.messages.filter((c) => { return c.id !== message.id })
                safeapply()
            })
        }

        /* load all the starred messages here */
        cacheSvc.getStarredCommentsDetails((results) => {
            results.forEach((c) => { c.starred = true; c.selected = false })
            $scope.messages = results
            $scope.$apply()
        })
    }

}

