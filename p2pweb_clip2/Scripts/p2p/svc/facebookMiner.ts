﻿class facebookMiner {
    public static $inject = ["cacheSvc"]
    constructor(private cacheSvc) {}

    /* public api, starts a mining operation, calls callback with results */
    startmine(activeKeywords, success) {
        const that = this

        this.cacheSvc.loadCache((cachedFeeds) => {
            /* filter out pages with no posts */
            cachedFeeds = cachedFeeds
                .filter((feed) => { return (feed.data && feed.data.data && feed.data.data.length) })
                .map((feed) => { return that.getSummaryObj(feed) })


            /* loop through each page */
            cachedFeeds.forEach((page) => {

                /* loop through each post on this page*/
                page.data.forEach((post) => {

                    /* loop through each comment on this post for keyword matches */
                    post.comments.data = post.comments.data.filter((comment) => {
                        return that.doesCommentMatchFilter(comment, activeKeywords)
                    })
                })

                /* remove posts with no matching comments */
                page.data = page.data.filter((post) => { return (post.comments.data.length > 0) })
            })


            /* remove pages with no posts */
            cachedFeeds = cachedFeeds.filter((page) => { return page.data.length > 0 })

            /* call success callback with the results */
            success(cachedFeeds)
        })

    }

    ///* public api, starts a mining operation, calls callback with results */
    //startmine_selectedfeeds(feeds, activeKeywords, success) {
    //    const that = this

    //    this.cacheSvc.loadCache((cachedFeeds) => {
    //        /* filter out pages with no posts */
    //        cachedFeeds = cachedFeeds
    //            .filter((feed) => { return (feed.data && feed.data.data && feed.data.data.length) })
    //            .map((feed) => { return that.getSummaryObj(feed) })


    //        /* loop through each page */
    //        cachedFeeds.forEach((page) => {

    //            /* loop through each post on this page*/
    //            page.data.forEach((post) => {

    //                /* loop through each comment on this post for keyword matches */
    //                post.comments.data = post.comments.data.filter((comment) => {
    //                    return that.doesCommentMatchFilter(comment, activeKeywords)
    //                })
    //            })

    //            /* remove posts with no matching comments */
    //            page.data = page.data.filter((post) => { return (post.comments.data.length > 0) })
    //        })


    //        /* remove pages with no posts */
    //        cachedFeeds = cachedFeeds.filter((page) => { return page.data.length > 0 })

    //        /* call success callback with the results */
    //        success(cachedFeeds)
    //    })

    //}


    /* case-insensitive search for keywords in comment */
    protected doesCommentMatchFilter(comment, keywords) {

        if (keywords.length == 0)
            return true

        const messageInLowerCase = comment.message.toLowerCase()
        let foundYet = false

        for (let idx = 0; idx < keywords.length && !foundYet; idx++) {
            const keywordInLowerCase = keywords[idx].toLowerCase()

            if (messageInLowerCase.indexOf(keywordInLowerCase) != -1) {
                foundYet = true
            }
        }

        return foundYet
    }

    /* object creation helper */
    protected getSummaryObj(feed) {
        return {
            id: feed.id,
            name: feed.name,
            data: feed.data.data.filter((post) => { return post.comments })
        }
    }

}
