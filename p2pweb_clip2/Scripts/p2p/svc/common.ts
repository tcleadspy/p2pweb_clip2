﻿
class common {


    searchArrayForID(array, id) {
        if (array.findIndex) {
            return array.findIndex(function (element) {
                return element.id == id
            })

        } else if (array.some) {
            var index = -1
            array.some(function (el, i) {
                if (el.id === id) {
                    index = i
                    return true
                }
            })

            return index
        } else {
            for (var i = 0; i < array.length; i++) {
                if (array[i].id === id)
                    return i
            }
        }

        return -1
    }

    ngTableFix(tableParams) {
        /* fix for ngTable when deleting last item on a given page
         * without fix, it gets stuck on empty page
         * fix just activates previous page (page() - 1)
        */
        if (tableParams.data.length == 1 && tableParams.page() != 1) {
            tableParams.page(tableParams.page() - 1);
        }
    }

    checkfbloaded(callback) {
        const that = this
        /* poll a global flag indicating facebook initialization complete */
        if ((<any>window).fbloaded)
            callback()
        else
            setTimeout(function () { that.checkfbloaded(callback) }, 1000)
    }

    safeApply(scope) {
        if (!scope.$$phase) {
            scope.$apply()
        }
    }
}
