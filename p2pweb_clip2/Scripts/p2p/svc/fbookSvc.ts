﻿


class fbookSvc {
    static inject = ['$rootScope', 'webapiSvc', 'appStateSvc']
    constructor(private $rootScope, private webapiSvc, private appStateSvc) { }

    private static qryFeedSmallOld: string = "/feed?fields=message,comments{like_count,from,id,message,created_time,is_hidden,comment_count},likes.limit(1).summary(true),updated_time&limit=50";
    private static qryFeedSmall2: string = "/feed?fields=message,comments{from, comments ,like_count,from,id,message,created_time,is_hidden,comment_count},likes.limit(1).summary(true),updated_time&limit=50";
    private static qryFeedSmall: string = "/feed?fields=message,comments{from, message, created_time, comments{from, message, created_time}},likes.limit(1).summary(true)&limit=50";
    private static qryFeedGroup: string = "/feed?fields=message&limit=50";
    private static qryFeedMsgOnly: string = "/feed?fields=message,updated_time&limit=50";
    private static qryPageInfo: string = "?fields=name,about,picture,category,fan_count,likes.limit(100)";
    private static qryGroupInfo: string = "?fields=name,privacy,icon,cover{source},description,email,owner,member_request_count,parent,updated_time";
    private static qryPostInfo: string = "?fields=message,likes.limit(1).summary(true),comments.limit(1).summary(true)";
    private static qrySearchPages: string = "search?q={0}&fields=name,about,picture,category,fan_count&limit=1000&type=Page";
    private static qrySearchGroups: string = "search?q={0}&fields=name,privacy,icon,cover{source},description,email,owner,member_request_count,parent,updated_time&limit=1000&type=Group";

    public static numCalls: number = 0;

    public static callDetails: string[] = [];

    public fbApi(qry: string, onSuccess: any, onError: any) {
        if (typeof (FB) === "undefined") {
            if (onError)
                onError()

            return
        }
        FB.api(qry, onSuccess);

        fbookSvc.numCalls++; //fbookSvc.numCalls() + 1);
        fbookSvc.callDetails.push(qry);
    }

    public getFeed(feedID: string, onSuccess: any, onError: any) {
        this.fbApi(feedID + fbookSvc.qryFeedSmall, onSuccess, onError);
    }

    public getFeedGroup(feedID: string, onSuccess: any, onError: any) {
        this.fbApi(feedID + fbookSvc.qryFeedGroup, onSuccess, onError);
    }

    public getPostInfo(postID: string, onSuccess: any): any {
        this.fbApi(postID + fbookSvc.qryPostInfo, onSuccess, null);
    }

    public getGroupInfo(groupID: string, onSuccess: any): any {
        this.fbApi(groupID + fbookSvc.qryGroupInfo, onSuccess, null);
    }

    public getPageInfo(pageID: string, onSuccess: any): any {
        this.fbApi(pageID + fbookSvc.qryPageInfo, onSuccess, null);
    }

    public searchForPages(searchTerm: string, onSuccess: any) {
        this.searchPagesOrGroups(
            fbookSvc.qrySearchPages.replace("{0}", searchTerm),
            onSuccess);
    }

    public searchForGroups(searchTerm: string, onSuccess: any) {
        this.searchPagesOrGroups(
            fbookSvc.qrySearchGroups.replace("{0}", searchTerm),
            onSuccess);
    }

    private searchPagesOrGroups(qry: string, onSuccess: any) {
        this.fbApi(qry, function (response) {
            const searchFeedsResults: any[] = [];

            if (response && response.data) {
                response.data.forEach(d => {
                    searchFeedsResults.push(d)
                });
            }

            onSuccess(searchFeedsResults);
        }, null);
    }

    public send(targetid, success, error) {
        const $rootScope = this.$rootScope
        const webapiSvc = this.webapiSvc

        FB.ui({
            to: targetid,
            app_id: $rootScope.app.app_id,
            method: 'send',
            link: this.appStateSvc.url4msgs
        },
            (result) => {
                if (!result) {
                    if (error) error("result=" + result)
                }
                else if (result.error_code) {
                    /* error_code 4201 means user canceled. just return */
                    if (result.error_code == 4201) return

                    if (error) error("error_code=" + result.error_code + " message=" + result.error_message)
                }
                else {
                    webapiSvc.addSentMessage(new sentmessageDTO(targetid))
                    if (success) success()
                }
            })
    }

}

