﻿class resultsFlattener {
    resultstoflatlist(pages) { return this.resultstoflatlist2(pages, this.getflatObj)  }

    resultstoflatlist2(pages, objFlattener) {
        const that = this
        const result = []

        pages.forEach((page) => {
            page.data.forEach((post) => {
                post.comments.data.forEach((comment) => {
                    result.push(that.getflatObj(page, post, comment))
                })
            })
        })

        return result
    }

    protected getflatObj(page, post, comment) {
        return {
            pageid: page.id,
            pagename: page.name,
            postid: post.id,
            commentid: comment.id,
            senderid: (comment.from) ? comment.from.id : '',
            statusmsgs: [],
            page: page,
            post: post,
            comment: comment
        }
    }
}
