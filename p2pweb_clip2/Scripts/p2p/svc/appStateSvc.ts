﻿class AppStateSvc {

    keywords: string[] = []
    feedsSelected: any[] = []
    loaded: boolean = false
    fbincorporated: boolean = true

    private mineViews: string[] = ['ngtable', 'ngtablegrouped', 'abntree1', 'nestedlist']
    mineView: string = 'nestedlist'
    url4msgs: string = ''

    constructor() { }

    applyResults(results) {
        this.keywords = results["keywords"]
        this.feedsSelected = results["pagewatches"].map(function (pw) {
            const obj = JSON.parse(pw.objJson)
            obj.busy = false
            obj.feedType = pw.feedType
            return obj
        })
        this.fbincorporated = (results["fbincorporated"].toString().toLowerCase() === "true")
        this.url4msgs = results["url4msgs"]
    }
}