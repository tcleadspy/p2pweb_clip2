﻿//declare var window: any;


class indexeddbCache {

    private static readonly dbName: string = "peer2prospect";
    private static readonly feedsTableName: string = "feeds";
    private static readonly queriesTableName: string = "queries";
    private static readonly savedCommentsTableName: string = "saved";
    private static readonly hideCommentsTableName: string = "hide";
    private static readonly sentMsgsTableName: string = "sent";
    private static readonly readCommentsTableName: string = "read";

    db: any;
    readComments: any = {}
    starredComments: any = {}

    /* initialization */
    constructor() { }
    initialize(successCallback) {
        var DBOpenRequest = window.indexedDB.open(indexeddbCache.dbName, 4);
        const self = this;
        DBOpenRequest.onerror = function (event) {
            throw "DBOpenRequest.onerror: " + event.toString();
        }
        DBOpenRequest.onsuccess = function (event) {
            self.db = (<any>(event.target)).result;
            self.loadReadComments(() => {
                self.loadStarredComments(() => {
                    successCallback()
                })
            })
        }
        DBOpenRequest.onupgradeneeded = function (event) {
            var db = (<any>(event.target)).result
            self.db = db
            db.onerror = function (event) {
                throw event;
            };
            (<any>(event.target)).transaction.oncomplete = function (event) {
                console.log('upgrade transaction complete')
            }

            var feedStore = db.createObjectStore(indexeddbCache.feedsTableName, { keyPath: "id" });
            var queriesStore = db.createObjectStore(indexeddbCache.queriesTableName, { autoIncrement: true });
            var savedCommentsStore = db.createObjectStore(indexeddbCache.savedCommentsTableName, { keyPath: "id" });
            var hideCommentsStore = db.createObjectStore(indexeddbCache.hideCommentsTableName, { keyPath: "id" });
            var sentMsgsStore = db.createObjectStore(indexeddbCache.sentMsgsTableName, { keyPath: "id" });
            var readMsgsStore = db.createObjectStore(indexeddbCache.readCommentsTableName, { keyPath: "id" });
        }
    }
    loadReadComments(success) {
        this.getAllKeys(indexeddbCache.readCommentsTableName, this.readComments, success)
    }
    loadStarredComments(success) {
        this.getAllKeys(indexeddbCache.savedCommentsTableName, this.starredComments, success)
    }
    getAllKeys(tablename, result, success) {
        const self = this

        const transaction = self.db.transaction([tablename], "readonly");
        transaction.oncomplete = function (event) {
            console.log('getAllKeys(' + tablename + ') - transaction complete');
            success()
        }
        transaction.onerror = function (event) {
            console.log('getAllKeys(' + tablename + ') - transaction error: ' + event);
        }

        const objectStore = transaction.objectStore(tablename);
        const objectStoreRequest = objectStore.getAllKeys()

        objectStoreRequest.onsuccess = function (event) {
            objectStoreRequest.result.forEach((key) => {
                result[key] = 1
            })
        }
    }


    /* general use */
    loadCache(success) {
        const tablename = indexeddbCache.feedsTableName;
        const self = this
        var transaction = self.db.transaction([tablename], "readonly");
        transaction.oncomplete = function (event) {
            console.log('loadCache transaction complete');
            const e = event;
        }
        transaction.onerror = function (event) {
            console.log('loadCache - transaction error');
            const e = event;
        }
        var objectStore = transaction.objectStore(tablename);
        var returnVal = [];
        objectStore.openCursor().onsuccess = function (event) {
            var cursor = event.target.result;
            if (cursor) {
                returnVal.push(cursor.value);
                cursor.continue();
            } else {
                console.log('all cache entries loaded. count: ' + returnVal.length);
                success(returnVal);
            }
        }
    }
    getReadComments() {
        const keys = Object.keys(this.readComments)
        return keys
    }
    getStarredComments() {
        const keys = Object.keys(this.starredComments)
        return keys
    }
    getStarredCommentsDetails(success) {
        const tablename = indexeddbCache.savedCommentsTableName
        const self = this
        const transaction = self.db.transaction([tablename], "readonly");
        transaction.oncomplete = function (event) {
            console.log('loadStarredComments transaction complete');
            if (success) success(returnVal)
        }
        transaction.onerror = function (event) {
            console.log('loadStarredComments - transaction error: ' + event);
        }
        const objectStore = transaction.objectStore(tablename);
        let returnVal = []
        objectStore.openCursor().onsuccess = function (event) {
            var cursor = event.target.result;
            if (cursor) {
                returnVal.push(cursor.value);
                cursor.continue();
            } else {
                console.log('all starred comment details loaded. count: ' + returnVal.length);
            }
        }
    }


    /* cruds no callback. just pass through */
    removeFeed(feedID: string) {
        this.deleteObjWithCallback(feedID, indexeddbCache.feedsTableName, undefined)
    }
    saveQry(qry) {
        this.addObjWithCallback(qry, indexeddbCache.queriesTableName, undefined)
    }
    saveFeed(feed) {
        this.addObjWithCallback(feed, indexeddbCache.feedsTableName, undefined)
    }


    /* cruds with callbacks */
    private addObjWithCallback(feed: any, tablename: string, success) {
        var transaction = this.db.transaction([tablename], "readwrite");
        transaction.oncomplete = function (event) {
            console.log('transaction complete adding to ' + tablename);
            if (success) success()
        }
        transaction.onerror = function (event) {
            console.log('transaction error adding to ' + tablename);
        }
        var objectStore = transaction.objectStore(tablename);
        var objectStoreRequest = objectStore.put(feed);
        objectStoreRequest.onsuccess = function (event) {
            console.log('addObj success adding to ' + tablename);
        }
    }
    private deleteObjWithCallback(feedID: string, tablename: string, success) {
        var transaction = this.db.transaction([tablename], "readwrite");
        transaction.oncomplete = function (event) {
            console.log('deleteObjWithCallback transaction complete on ' + tablename);
            if (success) success()
        }
        transaction.onerror = function (event) {
            console.log('deleteObjWithCallback transaction error on ' + tablename);
        }
        var objectStore = transaction.objectStore(tablename);
        var objectStoreRequest = objectStore.delete(feedID);
        objectStoreRequest.onsuccess = function (event) {
            console.log('deleteObjWithCallback success on ' + tablename);
        }
    }
    addSavedComment(comment, success) {
        const that = this
        this.addObjWithCallback(comment, indexeddbCache.savedCommentsTableName, () => {
            that.starredComments[comment.id] = 1
            success()
        })
    }
    removeSavedComment(commentID, success) {
        const that = this
        this.deleteObjWithCallback(commentID, indexeddbCache.savedCommentsTableName, () => {
            delete that.starredComments[commentID]
            success()
        })
    }
    addHiddenComment(commentID: string, success) {
        const obj = { id: commentID, value: commentID }
        this.addObjWithCallback(obj, indexeddbCache.hideCommentsTableName, success)
    }
    removeHiddenComment(commentID, success) {
        this.deleteObjWithCallback(commentID, indexeddbCache.hideCommentsTableName, success)
    }
    addReadComment(commentID, success) {
        const readDate = new Date()
        const obj = { id: commentID, date: readDate }
        this.addObjWithCallback(obj, indexeddbCache.readCommentsTableName, success)
        this.readComments[commentID] = readDate
    }
    removeReadComment(commentID) {
        this.deleteObjWithCallback(commentID, indexeddbCache.readCommentsTableName, undefined)
    }
    addSentMsg(msg, success) {
        // need to determine 'id' here... when sending a message... when is it created?
        this.addObjWithCallback(msg, indexeddbCache.savedCommentsTableName, success)
    }
    saveFeedWithCallback(feed, success) {
        this.addObjWithCallback(feed, indexeddbCache.feedsTableName, success)
    }
    removeFeedWithCallback(feedID: string, success: any) {
        this.deleteObjWithCallback(feedID, indexeddbCache.feedsTableName, success)
    }
    
}

