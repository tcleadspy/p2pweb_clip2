﻿class webapiSvc {
    static readonly pagesapiendpoint: string = "/api/pagewatches";
    static readonly groupsapiendpoint: string = "/api/groupwatches";
    static readonly keywordsapiendpoint: string = "/api/keywords";
    static readonly p2pusersettingsendpoint: string = "/api/p2pusersettings";
    static readonly useroptionsendpoint: string = "/api/useroptions";
    static readonly sentmessagesendpoint: string = "/api/sentmessages";

    testfun() { return "XXX" }

    removePageWatch(id, success) {
        const urlCall =
            webapiSvc.pagesapiendpoint;

        const self = this;
        jQuery.ajax({
            url: urlCall + '?' + jQuery.param({
                "id": id
                //, "bolDeleteReq": bolDeleteReq
            }),
            type: 'DELETE',
            success: success,
            //success: function () {
            //    self.keywords.remove(keyword);
            //    console.log('deleted keyword: ' + keyword)
            //},
            error: function (err) {
                console.log("Error");
                console.log(err);
            }
        });


    }

    getP2pusersettings(success) {

        jQuery.getJSON(webapiSvc.p2pusersettingsendpoint, success)

        //function (Data) {
        //    success(Data);
        //    //jQuery.each(Data.Keywords, function (key, val) {
        //    //    const newobj = JSON.parse(val.objJson);
        //    //    newpagesarray.push(newobj);
        //    //})
        //    //jQuery.each(Data, function (key, val) {
        //    //    const newobj = JSON.parse(val.objJson);
        //    //    newpagesarray.push(newobj);
        //    //})
        //})
    }
    
    addPageWatch(data, success) {
        const obj2Send = {
            id: data.id,
            objJson: JSON.stringify(data)
        }

        jQuery.post(webapiSvc.pagesapiendpoint, obj2Send)
            .done(function () {
                success();
            })
    }

    updateUserOptions(data, success) {
        const obj2Send = {
            fbincorporated: data.fbincorporated,
            url4msgs: data.url4msgs
        }

        jQuery.post(webapiSvc.useroptionsendpoint, obj2Send)
            .done(function () {
                success()
            })
    }

    getPageWatches(success) {
        jQuery.getJSON(webapiSvc.pagesapiendpoint, function (Data) {
            const newpagesarray = [];

            jQuery.each(Data, function (key, val) {
                const newobj = JSON.parse(val.objJson);
                newpagesarray.push(newobj);
            })

            success(newpagesarray);
        });
    }

    getGroupWatches(success) {
        jQuery.getJSON(webapiSvc.groupsapiendpoint,
            function (Data) {
                const newgroupsarray = [];

                jQuery.each(Data, function (key, val) {
                    const newobj = JSON.parse(val.objJson);
                    newgroupsarray.push(newobj);
                })

                success(newgroupsarray);
            });

    }

    addGroupWatch(data, success) {
        const obj = {
            id: data.id,
            objJson: JSON.stringify(data)
        };

        jQuery.post(webapiSvc.groupsapiendpoint, obj)
            .done(success());

    }

    removeGroupWatch(id: string, success: any) {
        const urlCall = webapiSvc.groupsapiendpoint;

        const self = this;
        jQuery.ajax({
            url: urlCall + '?' + jQuery.param({
                "id": id
                //, "bolDeleteReq": bolDeleteReq
            }),
            type: 'DELETE',
            success: success,
            //success: function () {
            //    self.keywords.remove(keyword);
            //    console.log('deleted keyword: ' + keyword)
            //},
            error: function (err) {
                console.log("Error");
                console.log(err);
            }
        });

    }

    removeKeyword(keyword: string, success) {
        const urlCall = webapiSvc.keywordsapiendpoint;

        const self = this;
        jQuery.ajax({
            url: urlCall + '?' + jQuery.param({
                "value": keyword
                //, "bolDeleteReq": bolDeleteReq
            }),
            type: 'DELETE',
            success: success,
            //success: function () {
            //    self.keywords.remove(keyword);
            //    console.log('deleted keyword: ' + keyword)
            //},
            error: function (err) {
                console.log("Error");
                console.log(err);
            }
        });

    }

    getKeywords(success) {
        jQuery.getJSON(webapiSvc.keywordsapiendpoint, function (Data) {
            const newkeywordsarray = [];

            jQuery.each(Data, function (key, val) {
                //const newobj = JSON.parse(val.keyword1);
                newkeywordsarray.push(val.keyword1);
            })

            success(newkeywordsarray);
        });
    }

    addKeyword(keyword: string, success) {
        const obj = {
            value: keyword
        }

        jQuery.post(webapiSvc.keywordsapiendpoint, obj)
            .done(function () {
                success();
            });
    }

    loadKeywordsFromServer(success) {
        jQuery.getJSON(webapiSvc.keywordsapiendpoint,
            function (Data) {
                const newkeywordsarray = [];

                jQuery.each(Data, function (key, val) {
                    newkeywordsarray.push(val.keyword1);
                })

                success(newkeywordsarray);
            });

    }

    getKeywordSuggestions(qry: string, success) {
        jQuery.getJSON("api/keywordsuggestions", { id: 1, value: qry })
            .done(function (Data) {
                const newsuggestions = [];

                jQuery.each(Data, function (key, val) {
                    newsuggestions.push(val);
                });

                success(newsuggestions);
            });
    }

    addSentMessage(msgDTO: sentmessageDTO, success) {
        const obj = {
            rcpt_fbid: msgDTO.rcpt_fbid
        }

        jQuery.post(webapiSvc.sentmessagesendpoint, obj)
            .done(function () {
                if (success) success()
            });
    }

}

