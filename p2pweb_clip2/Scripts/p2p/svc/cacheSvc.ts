﻿/*
centralize the cache service and abstract away the dependency on indexeddbCache
(in clients. there is an internal dependency in cacheSvc on indexeddbCache)

inject the cacheSvc into clients
when desired, replace the implementation transparently to clients
*/

interface iCacheSvc {
    initialize(successCallback)
    removeFeed(feedID: string)
    removeFeedWithCallback(feedID: string, success: any)
    loadCache(success)
    saveQry(qry)
    saveFeed(feed)
    saveFeedWithCallback(feed, success)
    addSavedComment(comment, success)
    addHiddenComment(commentID: string, success)
    addReadComment(commentID, success)
    addSentMsg(msg, success)
    removeSavedComment(commentID: string, success)
    removeHiddenComment(commentID: string, success)
    removeReadComment(commentID, success)
    getReadComments()
    getStarredComments()
    getStarredCommentsDetails(success)
}

class cacheSvc implements iCacheSvc {
    /* the hard-coded dependency. replace as desired. */
    private cache: iCacheSvc = new indexeddbCache()

    initialize(successCallback) { this.cache.initialize(successCallback) }
    removeFeed(feedID: string) { this.cache.removeFeed(feedID) }
    removeFeedWithCallback(feedID: string, success: any) { this.cache.removeFeedWithCallback(feedID, success) }
    loadCache(success) { this.cache.loadCache(success) }
    saveQry(qry) { this.cache.saveQry(qry) }
    saveFeed(feed) { this.cache.saveFeed(feed) }
    saveFeedWithCallback(feed, success) { this.cache.saveFeedWithCallback(feed, success) }
    addSavedComment(comment, success) { this.cache.addSavedComment(comment, success) }
    addHiddenComment(commentID: string, success) { this.cache.addHiddenComment(commentID, success) }
    addReadComment(commentID, success) { this.cache.addReadComment(commentID, success) }
    addSentMsg(msg, success) { this.cache.addSentMsg(msg, success) }
    getReadComments() { return this.cache.getReadComments() }
    removeSavedComment(commentID: string, success) { return this.cache.removeSavedComment(commentID, success) }
    removeHiddenComment(commentID: string, success) { return this.cache.removeHiddenComment(commentID, success) }
    removeReadComment(commentID: string, success) { return this.cache.removeReadComment(commentID, success) } 
    getStarredComments() { return this.cache.getStarredComments() }
    getStarredCommentsDetails(success) { return this.cache.getStarredCommentsDetails(success) }
}


/*
interfaces have no runtime representation, so in trying to figure out how to 'inject' an interface
as a dependency for others (and then to configure the injector with the associated implementation class)
an abstract base class was suggested.

this is not currently being used. instead there is an internal dependency in cacheSvc on indexeddbCache.
in other words, there is no 'angular dependency injection' going on in cacheSvc. it just manually
creates the indexeddbCache object.

abstract class cacheSvcBase {
    initialize(successCallback) { }
    saveKeyword(keyword: string) { }
    saveWatch(feed: any, tablename: string) { }
    removeFeed(feedID: string) { }
    removeFeedWithCallback(feedID: string, success: any) { }
    removePage(feedID: string) { }
    savePage(newFeed: any) { }
    removeGroup(feedID: string) { }
    loadCacheSummary(success) { }
    loadCache(success) { }
    saveQry(qry) { }
    saveFeed(feed) { }
    saveFeedWithCallback(feed, success) { }
}
*/
