﻿

class cacheMgrSvc {

    initialize = null

    public static $inject = ["fbookSvc", "cacheSvc", "appStateSvc", "common"]
    constructor(fbookSvc, cacheSvc, appStateSvc, common) {
        this.initialize = function ($scope, maxCacheAge) {
            cacheSvc.initialize(function () {
                /* load the cache to see which need refreshing */
                $scope.updateStatus("loading feed cache..")
                cacheSvc.loadCache(function (feedsInCache) {
                    processFeedsInCache($scope, feedsInCache)
                })
            })
        }

        const that = this

        /* update the global watch list's timestamp.. we receive the global list from the server,
         * but the server doesn't know the cache timestamp, so we're essentially merging that in here
        */
        const setwatchcachetimestamp = (watches, feed) => {
            const idx = common.searchArrayForID(watches, feed.id)
            switch (idx) {
                case -1:
                    return
                default:
                    watches[idx].timestamp = Date.parse(feed.timestamp)
            }
        }

        /* download the feed data for an array of facebook feeds */
        const downloadFeeds = (feeds2Download, $scope) => {
            /* determine if we have feeds to download */
            $scope.updateStatus("# feeds to download: " + feeds2Download.length)
            if (feeds2Download.length === 0) {
                /* no feeds to download, update 'loaded' flag */
                $scope.setReady(true)
                $scope.percentFinished = 100
                return
            }

            /*syncing hack to know when done downloading */
            const origNumExpected = feeds2Download.length
            let numExpected = origNumExpected

            /* begin downloads */
            $scope.percentFinished = 80
            feeds2Download.forEach(function (feed) {
                /* download one feed */
                $scope.updateStatus("downloading feed: " + feed.name)
                fbookSvc.getFeed(feed.id, function (data) {
                    /* 
                    null data maybe needs to be handled differently
                    if we simply don't save it, then we'll re-download every time
                    */
                    if (data !== null) {
                        /* save feed contents to local cache */
                        $scope.updateStatus("refreshing feed: " + feed.name)
                        cacheSvc.saveFeed(new feedCacheEntry(feed, data))
                        setwatchcachetimestamp(appStateSvc.feedsSelected, feed)
                    }
                    /* check if we are done */
                    numExpected--
                    $scope.$apply(function () {
                        $scope.percentFinished = Math.round((origNumExpected - (origNumExpected - numExpected)) / origNumExpected)
                    })
                    if (numExpected === 0) {
                        /* done */
                        $scope.updateStatus("done with feed downloads...")
                        $scope.setReady(true)
                    }
                })
            })
        }

        /* refresh/delete cache entries as needed */
        const processFeedsInCache = ($scope, feedsInCache) => {
            /* feedsInCache (callback param) now holds the contents of the local cache */
            /* feedsSelected holds the global watch list from the server */
            const feedsSelected = appStateSvc.feedsSelected
            /* for each local cache entry,
             * verify it's still selected based on global watch list
             * if still selected, update the timestamp in the global watch list based on the cache timestamp
             * if not still selected, remove it from local cache
             */
            feedsInCache.forEach(function (feedInCache) {
                /* verify it's still selected based on global watch list */
                const idx = common.searchArrayForID(feedsSelected, feedInCache.id)
                /* if it's still selected (idx !== -1) meaning we found it in above search*/
                if (idx !== -1)
                    /* update the timestamp on the global watch list */
                    setwatchcachetimestamp(feedsSelected, feedInCache)
                else
                    /* it wasn't found, ie. is no longer selected. remove from cache */
                    cacheSvc.removeFeed(feedInCache.id)
            })
            /* determine the feeds to refresh and the feeds to delete from local cache */
            const cleanupInfo = getFeedCleanupInfo(feedsInCache, appStateSvc.feedsSelected, $scope.maxCacheAgeInHrs)
            const feeds2Delete = cleanupInfo.feeds2Delete
            /* delete feeds from cache */
            $scope.updateStatus("# of feeds to delete from cache: " + feeds2Delete.length)
            console.log('# of feeds to delete from cache: ' + feeds2Delete.length)
            feeds2Delete.forEach(function (feed) {
                cacheSvc.removeFeed(feed.id)
            })
            /* checkfbloaded polls for facebook initialization to complete */
            $scope.updateStatus("checking facebook api status...")
            common.checkfbloaded(function () {
                /* facebook initialization complete */
                $scope.updateStatus("facebook api loaded...")
                downloadFeeds(cleanupInfo.feeds2Download, $scope)
            })
        }

        /* determine feeds to delete and feeds to download */
        const getFeedCleanupInfo = (feeds, pagewatches, maxCacheAgeInHrs) => {
            const rightNow = new Date()
            const cutoffDate = rightNow.setHours(rightNow.getHours() - maxCacheAgeInHrs)

            /* determine expired or orphaned feeds to delete from cache */
            const feeds2Delete = feeds.filter(function (feed) {
                return (
                    Date.parse(feed.timestamp) < cutoffDate
                    || common.searchArrayForID(pagewatches, feed.id) === -1
                )
            })

            /* determine expired, non-orphaned feeds which will need to be refreshed */
            const feeds2Refresh = feeds.filter(function (feed) {
                return (
                    Date.parse(feed.timestamp) < cutoffDate
                    && common.searchArrayForID(pagewatches, feed.id) !== -1
                )
            })

            /* determine if we have missing cache entries. this can happen if using multiple devices */
            const feedsMissing = pagewatches.filter(function (pagewatch) {
                return (common.searchArrayForID(feeds, pagewatch.id) === -1)
            })

            /* combine list of feeds that are missing, and list of feeds needing refreshing */
            const feeds2Download = feedsMissing.concat(feeds2Refresh)

            return { feeds2Delete: feeds2Delete, feeds2Download: feeds2Download }
        }
    }


}