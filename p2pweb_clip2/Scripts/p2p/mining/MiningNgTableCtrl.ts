﻿class MiningNgTableCtrl extends MiningNgTableBaseCtrl {

    public static $inject = ['$rootScope', "$scope", "$log", "NgTableParams", "ngTableEventsChannel", 'fbookSvc', 'resultsFlattener']
    constructor($rootScope, $scope, $log, NgTableParams, ngTableEventsChannel, fbookSvc, resultsFlattener) {
        super($scope, $log, $rootScope, NgTableParams, ngTableEventsChannel, fbookSvc, resultsFlattener)

        this.$scope.$emit('viewready', this)
    }

    resetTable() {
        // $scope.tableParams.reload();

        this.$scope.tableParams = new this.NgTableParams({
            page: 1, // show first page
            count: this.$rootScope.pageSize, // count per page
            //group: "pageid",
            //// initial sort order
            sorting: {
                commentid: "desc"
            }
        }, {
                total: this.my_data.length, //data.length, // length of data
                dataset: this.my_data// results
            });
    }
}


