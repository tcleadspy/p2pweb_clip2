﻿/* data class to hold view info */
class miningView { constructor(public name: string, public url: string) { } }

/*
top-level controller for the mining page.
handles the common functionality among mining views including entry of
custom keyword and determination of activeKeywords to use for mining.
allows dynamic selection of view layout (each view will have its own controller).
*/
class MiningViewWrapperCtrl {
    private readonly prefix = '/Scripts/p2p/mining/views/'
    availableViews = [
        new miningView('ngtablegrouped', this.prefix + 'ngtable_grouped.html'),
        new miningView('ngtable', this.prefix + 'ngtable.html'),
        new miningView('nestedlist', this.prefix + 'nestedlist.html'),
        new miningView('abntree1', this.prefix + 'abntree1.html}')
    ]
    selectedView: miningView = null
    customKeyword = ''

    static $inject = ['$rootScope', '$scope', 'facebookMiner', 'appStateSvc']
    constructor(private $rootScope, private $scope, private facebookMiner: facebookMiner, private appStateSvc: AppStateSvc) {
        const vm = this

        /* set to the default view (inherited via $rootScope.app.layout.mineView) */
        vm.selectedView = vm.availableViews.filter((v) => { return v.name === $rootScope.app.layout.mineView })[0]

        /* when the local selectedView property changes, update the app-level object */
        $scope.$watch('vm.selectedView', (newval, oldval) => {
            if (newval && newval.name) {
                $rootScope.app.layout.mineView = newval.name
            }
        })

        /* when the child view is ready, run a mining operation automatically */
        $scope.$on('viewready', (childController) => { vm.mine() })
    }

    /* start a mining operation and broadcast minecomplete event on completion */
    mine() {
        const vm = this

        /* if custom keyword entered, use (as an array), otherwise use defaults */
        const activeKeywords = (vm.customKeyword !== '') ? [vm.customKeyword] : this.appStateSvc.keywords

        vm.facebookMiner.startmine(activeKeywords, (results) => {
            vm.$scope.$broadcast('minecomplete', results)
        })
    }
}

