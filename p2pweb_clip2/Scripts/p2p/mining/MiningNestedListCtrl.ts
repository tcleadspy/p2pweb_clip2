﻿
/* 
controller for nested list view on the mining page
*/
class MiningNestedListCtrl {
    private my_data = []

    public static $inject = ['$rootScope', '$scope', '$log', 'fbookSvc']
    constructor(private $rootScope, private $scope, private $log, private fbookSvc) {
        const that = this
        this.$scope.$on('minecomplete', (sender, results) => {
            $log.log('minecomplete event received')
            that.$scope.$apply(() => {
                that.my_data = that.resultstoflatlist(results)
            })
        })

        this.$scope.$emit('viewready', this)
    }


    /* convert the results graph (tree) to a flat list */
    private resultstoflatlist (pages) {
        var result = []
        const that = this

        pages.forEach(function (page) {
            page.data.forEach(function (post) {
                post.comments.data.forEach(function (comment) {
                    result.push(that.getflatObj(page, post, comment))
                })
            })
        })

        return result
    }


    /* object creation helper */
    private getflatObj (page, post, comment) {
        return {
            page: page,
            post: post,
            comment: comment
        }
    }


    private sendmessage(comment) {
        this.fbookSvc.send(comment.comment.from.id, () => {
            this.$log.log('success')
        }, () => {
            this.$log.log('error on the sent messages save')
        })
    }
}

