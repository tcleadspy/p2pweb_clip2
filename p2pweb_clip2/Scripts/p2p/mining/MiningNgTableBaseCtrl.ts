﻿/*
base class for ngTable-based mining views
*/
abstract class MiningNgTableBaseCtrl {
    protected my_data = []
    protected selected = null
    protected widescreen = false

    public static $inject = ['$rootScope', "$scope", "$log", "NgTableParams", "ngTableEventsChannel", 'fbookSvc', 'resultsFlattener']
    constructor(protected $scope, protected $log, protected $rootScope, protected NgTableParams, protected ngTableEventsChannel, protected fbookSvc: fbookSvc, protected flattener: resultsFlattener) {
        const that = this

        ngTableEventsChannel.onAfterReloadData(_.partial($rootScope.logEvent, $rootScope, $scope), $scope);

        // media query event handler
        if (matchMedia) {
            const mq = window.matchMedia("(min-width: 800px)");
            mq.addListener(WidthChange);
            WidthChange(mq);
        }

        function WidthChange(mq) { that.widescreen = mq.matches }

        this.$scope.$on('minecomplete', (sender, results) => {
            $log.log('minecomplete event received')
            that.$scope.$apply(() => {
                that.my_data = flattener.resultstoflatlist(results)
                that.resetTable()
            })
        })
    }

    protected sendmessage(commentWrapper) {
        const log = this.$log.log
        const comment = commentWrapper.comment
        this.fbookSvc.send(comment.from.id, () => {
            log('message sent to ' + comment.from.id + ':' + comment.from.name)
        }, (msg) => {
            alert('error occurred while sending.' + msg)
        })
    }

    protected resetTable() {
        // $scope.tableParams.reload();

        this.$scope.tableParams = new this.NgTableParams({
            page: 1, // show first page
            count: this.$rootScope.pageSize, // count per page
            //group: "pageid",
            //// initial sort order
            //sorting: {
            //    name: "desc"
            //}
        }, {
                total: this.my_data.length, //data.length, // length of data
                dataset: this.my_data// results
            });

    }

    protected selectComment(comment) {
        comment.selected = !comment.selected
    }


}


