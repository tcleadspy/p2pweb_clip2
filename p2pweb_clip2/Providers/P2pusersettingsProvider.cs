﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using p2pweb_clip2.Models.dto;
using p2pweb_clip2.Models.edm;

namespace p2pweb_clip2.Providers
{
    public class P2pusersettingsProvider
    {
        public static P2pUserSettingsDTO getsettings(string username)
        {
            using (var db = new Entities())
            {
                var qry =
                    from gw1
                    in db.PageWatches
                    join feedblob in db.FeedBlobs on gw1.feedid equals feedblob.id
                    where gw1.username == username // User.Identity.Name
                    select new FBPageJsonDTO() { id = feedblob.id, objJson = feedblob.obj, feedType = "page"};


                var userPageWatches = qry.ToList();

                var grpsqry =
                    from gw1
                    in db.GroupWatches
                    join feedBlob in db.FeedBlobs on gw1.feedid equals feedBlob.id
                    where gw1.username == username
                    select new FBPageJsonDTO() { id = feedBlob.id, objJson = feedBlob.obj, feedType = "group" };

                userPageWatches.AddRange(grpsqry.ToList());


                var keywords =
                    db
                    .SearchTerms
                    .Where((kw) => kw.username == username) // User.Identity.Name)
                    .Select((kw) => kw.keyword1)
                    .ToList();

                var sentmessages =
                    db
                    .SentMessages
                    .Where((sm) => sm.username == username)
                    .Select((sm) => new SentMessageDTO()
                    {
                        rcpt_fbid = sm.rcpt_fbid,
                        sent_date = sm.sent_date
                    }).ToList();

                var p2pusersettings = new P2pUserSettingsDTO()
                {
                    username = username,
                    pagewatches = userPageWatches,
                    sentmsgs = sentmessages,
                    appid = Properties.Settings.Default.appid,
                    keywords = keywords
                };

                var userOptionsOrDefault = P2pusersettingsProvider.getUserOptionsOrDefault(db, username);
                p2pusersettings.facebookIntegration = userOptionsOrDefault.fb_incorporation_on;
                p2pusersettings.url4msgs = userOptionsOrDefault.url;

                return p2pusersettings;
            }
        }

        private static UserOption getUserOptionsOrDefault(Entities db, string username)
        {
            UserOption userOptions =
                db
                .UserOptions
                .Where(uo => uo.username == username)
                .SingleOrDefault();


            if (userOptions == null)
            {
                userOptions = new UserOption();
                userOptions.username = username;
                userOptions.fb_incorporation_on = true;
                userOptions.url = "";
                db.UserOptions.Add(userOptions);
                db.SaveChanges();
            }

            return userOptions;
        }


        //public ActionResult Index()
        //{
        //    p2pweb.Models.UserOptions returnVal = null;

        //    using (var db = new Entities())
        //    {
        //        UserOption theitem = null;

        //        theitem = db
        //            .UserOptions
        //            .Where(uo => uo.username == User.Identity.Name)
        //            .SingleOrDefault();

        //        if (theitem == null)
        //        {
        //            theitem = new UserOption();
        //            theitem.username = User.Identity.Name;
        //            theitem.fb_incorporation_on = true;
        //            db.UserOptions.Add(theitem);
        //            db.SaveChanges();
        //        }

        //        returnVal = new p2pweb.Models.UserOptions(theitem);
        //    }

        //    return View(returnVal);
        //}

    }
}