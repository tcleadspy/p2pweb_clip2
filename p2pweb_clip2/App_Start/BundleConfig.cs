﻿using System.Web;
using System.Web.Optimization;

namespace p2pweb_clip2
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            //if (Properties.Settings.Default.force_minification == true)
            //{
            //    BundleTable.EnableOptimizations = true;
            //}

            BundleTable.EnableOptimizations = false;

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));

            bundles.Add(new ScriptBundle("~/bundles/optimizetest").Include(
                "~/Scripts/p2p/jsgen/optimizetest/optimizetestCtrl.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/common").Include(
                "~/Scripts/p2p/jsgen/svc/common.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/cliptwoapp").Include(
                "~/Scripts/p2p/jsgen/svc/resultsFlattener.js",
                "~/Scripts/p2p/jsgen/svc/webapiSvc.js",
                "~/Scripts/p2p/jsgen/svc/indexeddbCache.js",
                "~/Scripts/p2p/jsgen/svc/cacheSvc.js",
                "~/Scripts/p2p/jsgen/misc/dto.js",
                "~/Scripts/p2p/jsgen/svc/common.js",
                "~/Scripts/p2p/jsgen/misc/fbwatch.js",
                "~/Scripts/p2p/jsgen/svc/facebookMiner.js",
                "~/Scripts/p2p/jsgen/svc/appStateSvc.js",
                "~/Scripts/p2p/jsgen/svc/cacheMgrSvc.js",
                "~/Scripts/p2p/jsgen/retro/retroCtrl.js",
                "~/Scripts/p2p/jsgen/dashboard/p2pdashboardCtrl.js",
                "~/Scripts/p2p/jsgen/starred/starredCtrl.js",
                "~/Scripts/p2p/jsgen/feeds/facebookFeedsCtrl.js",
                "~/Scripts/p2p/jsgen/feedsearch/facebookFeedSearchCtrl.js",
                "~/Scripts/p2p/jsgen/keywords/keywordsCtrl.js",
                "~/Scripts/p2p/jsgen/usersettings/p2pUserSettingsCtrl.js",
                "~/Scripts/p2p/jsgen/mining/MiningViewWrapperCtrl.js",
                "~/Scripts/p2p/jsgen/mining/MiningNestedListCtrl.js",
                "~/Scripts/p2p/jsgen/mining/MiningNgTableBaseCtrl.js",
                "~/Scripts/p2p/jsgen/mining/MiningNgTableCtrl.js",
                "~/Scripts/p2p/jsgen/mining/MiningNgTableGroupCtrl.js",
                "~/Scripts/p2p/jsgen/svc/fbookSvc.js",
                "~/Scripts/assets/js/appDependencies.js",
                //"~/Scripts/assets/js/app.js",
                "~/Scripts/p2p/app/script/app.js",
                "~/Scripts/p2p/app/script/p2pappCtrl.js",
                "~/Scripts/assets/js/config.constant.js",
                "~/Scripts/assets/js/config.router.js")
                .IncludeDirectory("~/Scripts/assets/js/directives", "*.js")
                .Include(
                "~/Scripts/p2p/jsgen/misc/webapi.js",
                "~/Scripts/assets/js/controllers/appCtrl.js",
                "~/Scripts/assets/js/controllers/inboxCtrl.js",
                "~/Scripts/assets/js/controllers/bootstrapCtrl.js",
                "~/Scripts/p2p/jsgen/misc/fbook.js"
                //"~/Scripts/assets/js/controllers/miningCtrl.js",
                //"~/Scripts/assets/js/controllers/miningctrltest.js",
                //"~/Scripts/assets/js/controllers/miningCtrlNestedList.js"
            ));

        }
    }
}
