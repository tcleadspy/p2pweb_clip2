﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace p2pweb_clip2.Controllers
{
    [RequireHttps]
    [System.Web.Mvc.Authorize]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            var tmp = 
                p2pweb_clip2
                .Providers
                .P2pusersettingsProvider
                .getsettings(User.Identity.Name);

            return View(tmp);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View(false);
        }
    }
}