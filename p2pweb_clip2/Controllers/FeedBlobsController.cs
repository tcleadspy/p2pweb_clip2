﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using p2pweb;
using p2p_web_ng;
using p2pweb_clip2;
using System.Web.Mvc;
using p2pweb_clip2.Models.edm;

namespace p2pweb.Controllers
{
    [RequireHttps]
    [System.Web.Http.Authorize]
    public class FeedBlobsController : ApiController
    {
        private Entities db = new Entities();

        // GET: api/FeedBlobs
        public IQueryable<FeedBlob> GetFeedBlobs()
        {
            return db.FeedBlobs; //.Where((fb) => fb.;
        }

        // GET: api/FeedBlobs/5
        [ResponseType(typeof(FeedBlob))]
        public IHttpActionResult GetFeedBlob(string id)
        {
            FeedBlob feedBlob = db.FeedBlobs.Find(id);
            if (feedBlob == null)
            {
                return NotFound();
            }

            return Ok(feedBlob);
        }

        // PUT: api/FeedBlobs/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutFeedBlob(string id, FeedBlob feedBlob)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != feedBlob.id)
            {
                return BadRequest();
            }

            db.Entry(feedBlob).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FeedBlobExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/FeedBlobs
        [ResponseType(typeof(FeedBlob))]
        public IHttpActionResult PostFeedBlob(FeedBlob feedBlob)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.FeedBlobs.Add(feedBlob);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (FeedBlobExists(feedBlob.id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = feedBlob.id }, feedBlob);
        }

        // DELETE: api/FeedBlobs/5
        [ResponseType(typeof(FeedBlob))]
        public IHttpActionResult DeleteFeedBlob(string id)
        {
            FeedBlob feedBlob = db.FeedBlobs.Find(id);
            if (feedBlob == null)
            {
                return NotFound();
            }

            db.FeedBlobs.Remove(feedBlob);
            db.SaveChanges();

            return Ok(feedBlob);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool FeedBlobExists(string id)
        {
            return db.FeedBlobs.Count(e => e.id == id) > 0;
        }
    }
}