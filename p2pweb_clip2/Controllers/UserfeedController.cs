﻿using p2p_web_ng;
using p2pweb_clip2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using p2pweb_clip2.Models.edm;

namespace p2pweb.Controllers
{
    [RequireHttps]
    [System.Web.Http.Authorize]
    public class UserfeedController : ApiController
    {
        // GET api/<controller>
        public IEnumerable<string> Get()
        {
            var tmp = User.Identity.Name;
            var db = new Entities();
            var feeds = db.AspNetUsers.Select(u => u.UserName == tmp);

            return new string[] { "value1", "value2" };
        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}