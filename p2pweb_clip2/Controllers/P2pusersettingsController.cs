﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using p2pweb_clip2;
using System.Web.Mvc;
using p2pweb_clip2.Models.dto;

namespace p2p_web_ng.Controllers
{
    [RequireHttps]
    [System.Web.Http.Authorize]
    public class P2pusersettingsController : ApiController
    {

        public P2pUserSettingsDTO Get()
        {
            var tmp = 
                p2pweb_clip2
                .Providers
                .P2pusersettingsProvider
                .getsettings(User.Identity.Name);

            return tmp;
        }

        //public void Post([FromBody]P2pUserSettingsDTO dto)
        //{
        //    using (var db = new Entities())
        //    {
        //        var newlog = new log();
        //        var msg2 = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(dto);
        //        newlog.msg = "P2pusersettingsController::Post - dto=" + msg2;
        //        db.logs.Add(newlog);
        //        db.SaveChanges();
        //    }
        //    p2pweb_clip2.Providers.P2pusersettingsProvider.updateSettings(dto, User.Identity.Name);
        //}
    }
}
