﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using System.Xml;

namespace p2pweb.Controllers
{
    [RequireHttps]
    [System.Web.Http.Authorize]
    public class KeywordsuggestionsController : ApiController
    {

        //public List<string> Get(string qry)
        //{
        //    try
        //    {
        //        var suggestUrl =
        //            "http://suggestqueries.google.com/complete/search?output=toolbar&hl=en&q=";

        //        var xmlreader =
        //            new XmlTextReader(suggestUrl + qry);

        //        var suggestions = new List<string>();

        //        while (xmlreader.Read())
        //        {
        //            switch (xmlreader.Name)
        //            {
        //                case "suggestion":
        //                    var Text = xmlreader.GetAttribute("data");
        //                    var ID = xmlreader.GetAttribute("value");
        //                    suggestions.Add(Text);
        //                    break;
        //            }
        //        }

        //        xmlreader.Close();

        //        return suggestions;
        //    }
        //    catch (Exception ex)
        //    {
        //        return new List<string>()
        //        {
        //            ex.Message
        //        };
        //    }
        //}

        public List<string> Get([FromUri]StringWrapper stringWrapper)
        {
            try
            {
                var qry = stringWrapper.value;

                var suggestUrl =
                    "https://suggestqueries.google.com/complete/search?output=toolbar&hl=en&q=";

                var xmlreader =
                    new XmlTextReader(suggestUrl + qry);

                var suggestions = new List<string>();

                while (xmlreader.Read())
                {
                    switch (xmlreader.Name)
                    {
                        case "suggestion":
                            var Text = xmlreader.GetAttribute("data");
                            var ID = xmlreader.GetAttribute("value");
                            suggestions.Add(Text);
                            break;
                    }
                }

                xmlreader.Close();

                return suggestions;
            }
            catch (Exception ex)
            {
                return new List<string>() { ex.Message };
            }
        }
    }
}
