﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json;
using p2p_web_ng;
using p2pweb_clip2;
using System.Web.Mvc;
using p2pweb_clip2.Models.dto;
using p2pweb_clip2.Models.edm;

namespace p2pweb.Controllers
{
    [RequireHttps]
    [System.Web.Http.Authorize]
    public class PagewatchesController : ApiController
    {
        private Entities db = new Entities();

        public IEnumerable<FBPageJsonDTO> Get()
        {
            var qry =
                from
                    gw1 in db.PageWatches
                    join feedblob in db.FeedBlobs on gw1.feedid equals feedblob.id
                where
                    gw1.username == User.Identity.Name
                select
                    new FBPageJsonDTO()
                    {
                        id = feedblob.id
                        , objJson = feedblob.obj
                    };

            var watches = 
                qry.ToList();

            return watches;

            //var qry =
            //    from
            //        gw1 in db.GroupWatches
            //        join feed in db.Feeds on gw1.feedid equals feed.feedid
            //    where
            //        gw1.username == User.Identity.Name
            //    select
            //        new FBPage()
            //        {
            //            feedid = feed.feedid
            //            ,
            //            category = feed.category
            //            ,
            //            name = feed.name
            //            ,
            //            fan_count = feed.fan_count
            //        };

            //var watches = qry.ToList();

            //return watches;

        }

        // POST api/<controller>
        public void Post([FromBody]FBPageJsonDTO page)
        {
            if (User.Identity.Name == null)
                return;

            var existing =
                db
                .GroupWatches
                .Any((gw) => gw.feedid == page.id && gw.username == User.Identity.Name);

            if (existing)
                return;

            //var existingFeed = 
            //    db.Feeds.Any((f) => f.feedid == page.id);

            var existingFeedBlob =
                db.FeedBlobs.Any((f) => f.id == page.id);

            try
            {
                using (var db = new Entities())
                {
                    if (!existingFeedBlob)
                    {
                        var newFeedBlob = new FeedBlob()
                        {
                            id = page.id
                             ,
                            obj = page.objJson
                        };

                        db.FeedBlobs.Add(newFeedBlob);
                    }
                    else
                    {
                        var existingBlob = db.FeedBlobs.Single((f) => f.id == page.id);
                        existingBlob.obj = page.objJson;
                    }

                    var newpagewatch = new PageWatch()
                    {
                        feedid = page.id
                         ,
                        username = User.Identity.Name
                    };

                    db.PageWatches.Add(newpagewatch);

                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                var tmp = ex;
            }
        }

        public void Delete([FromUri]string id)
        {
            if (id == null)
                throw new Exception("PagewatchesController::Delete called with null id");

            using (var db = new Entities())
            {
                var objs = db.PageWatches
                    .Where((p) => p.feedid == id && p.username == User.Identity.Name);


                foreach (var obj in objs)
                {
                    db.PageWatches.Remove(obj);
                }

                db.SaveChanges();
            }
        }

        //public void Delete([FromUri]StringWrapper stringWrapper)
        //{
        //    var id = stringWrapper.value;

        //    using (var db = new Entities())
        //    {
        //        var objs = db.PageWatches
        //            .Where((p) => p.feedid == id && p.username == User.Identity.Name);


        //        foreach(var obj in objs)
        //        {
        //            db.PageWatches.Remove(obj);
        //        }

        //        db.SaveChanges();
        //    }
        //}

        //public Item GetItemById(int id)
        //{
        //    var item = items.FirstOrDefault((I) => I.Id == id);
        //    if (item == null)
        //    {
        //        throw new HttpResponseException(HttpStatusCode.NotFound);
        //    }
        //    return item;
        //}
        //public IEnumerable<Item> GetItemsByType(string type)
        //{

        //    return items.Where(
        //        (I) => string.Equals(I.Type, type,
        //            StringComparison.OrdinalIgnoreCase));
        //}
    }

}
