﻿using p2pweb_clip2.Models.edm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Mvc;

namespace p2pweb.Controllers {
    [RequireHttps]
    [System.Web.Http.Authorize]
    public class KeywordsController : ApiController
    {
        public List<SearchTerm> Get()
        {
            if (User.Identity.Name == null)
                return null;

            var db = new Entities();

            var keywords =
                db
                .SearchTerms
                .Where(u => u.username == User.Identity.Name)
                .ToList();

            return keywords;
        }

        // POST api/<controller>
        public void Post([FromBody]StringWrapper stringWrapper)
        {
            if (stringWrapper == null)
            {
                System.Console.WriteLine("keyword post, value was null");
                return;
            }

            System.Console.WriteLine("KeywordsPost, value is " + stringWrapper);
            var db = new Entities();

            var newkeyword = new SearchTerm() {
                keyword1 = stringWrapper.value
                , username = User.Identity.Name
            };

            db.SearchTerms.Add(newkeyword);

            db.SaveChanges();
        }

        public void Delete([FromUri]StringWrapper stringWrapper)
        {
            if (stringWrapper == null)
            {
                return;
            }

            try
            {
                var db = new Entities();
                var objs =
                    db.SearchTerms
                    .Where((k) => k.keyword1 == stringWrapper.value)
                    .ToList();

                foreach(var obj in objs)
                {
                    db.SearchTerms.Remove(obj);
                }
                db.SaveChanges();
                //var db = new Entities();
                //var obj =
                //    db.Keywords
                //    .Where((k) => k.keyword1 == stringWrapper.value)
                //    .Single();

                //db.Keywords.Remove(obj);
                //db.SaveChanges();
            }
            catch (InvalidOperationException)
            {}
        }

        //public List<string> Get()
        //{
        //    var tmp = User.Identity.Name;
        //    var db = new Entities();

        //    var keywords =
        //        db
        //        .Keywords
        //        .Where(u => u.username == tmp)
        //        .ToList();

        //    var responseList = new List<string>() { };

        //    foreach (var keywd in keywords)
        //    {
        //        responseList.Add(keywd.keyword1);
        //    }

        //    return responseList;
        //    //return keywords.Select new { ;
        //    //return new List<string>() { "value1", "value2" };
        //}
    }

    public class StringWrapper
    {
        public string value { get; set; }
    }
}
