﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace p2pweb_clip2.Controllers
{
    /* helper just to troubleshoot initial sql connections without requiring angular prep */
    /* NOT currently active. uncomment to activate */

    public class TestController : Controller
    {
        //// GET: SqlTest
        //public ActionResult Index()
        //{
        //    try
        //    {
        //        var tmp =
        //            p2pweb_clip2
        //            .Providers
        //            .P2pusersettingsProvider
        //            .getsettings(User.Identity.Name);

        //        return View();
        //    }
        //    catch (Exception ex)
        //    {
        //        ViewBag.ex = ex;
        //        ViewBag.msg = ex.Message;
        //        return View();
        //    }
        //}

        public ActionResult CssTest()
        {
            return View();
        }

        public ActionResult CssTest2()
        {
            var tmp =
                p2pweb_clip2
                .Providers
                .P2pusersettingsProvider
                .getsettings("quamv");

            return View(tmp);
        }

    }
}