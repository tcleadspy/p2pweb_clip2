﻿using p2pweb_clip2.Models.dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using p2pweb_clip2.Models.edm;

namespace p2pweb_clip2.Controllers
{
    public class UseroptionsController : ApiController
    {
        private static string defaultUrl = "https://www.peer2prospect.com";

        // GET api/<controller>
        public UserOption Get()
        {
            using (var db = new Entities())
            {
                return (
                    db
                    .UserOptions
                    .Where(uo => uo.username == User.Identity.Name)
                    .Single()
                );
            }
        }

        // POST api/<controller>
        public void Post([FromBody]UserOptionsDTO dto)
        {
            using (var db = new Entities())
            {
                var newlog1 = new log() { msg = "dto.fbincorporated" + dto.fbincorporated };
                db.logs.Add(newlog1);
                db.SaveChanges();

                var newlog = new log();
                var msg2 = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(dto);
                newlog.msg = "P2pusersettingsController::Post - dto=" + msg2;
                db.logs.Add(newlog);
                db.SaveChanges();

                var dbrec =
                    db
                    .UserOptions
                    .Where(uo2 => uo2.username == User.Identity.Name)
                    .SingleOrDefault();

                if (dbrec == null)
                {
                    var obj = new UserOption()
                    {
                        fb_incorporation_on = dto.fbincorporated,
                        url = UseroptionsController.defaultUrl,
                        username = User.Identity.Name
                    };
                    db.UserOptions.Add(obj);
                    db.SaveChanges();
                }else
                {
                    dbrec.fb_incorporation_on = dto.fbincorporated;
                    dbrec.url = dto.url4msgs;
                    db.SaveChanges();
                }

            }
        }

        //public static void updateSettings(P2pUserSettingsDTO dto, string username)
        //{
        //    using (var db = new Entities())
        //    {
        //        var rec = db.UserOptions.Where(uo => uo.username == username).Single();
        //        rec.fb_incorporation_on = dto.facebookIntegration;
        //    }
        //}



        //// PUT api/<controller>/5
        //public void Put(int id, [FromBody]string value) { }
        //// DELETE api/<controller>/5
        //public void Delete(int id){}
    }
}