﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using p2pweb;
using p2p_web_ng;
using p2pweb_clip2;
using System.Web.Mvc;
using p2pweb_clip2.Models.edm;

namespace p2pweb.Controllers
{
    [RequireHttps]
    [System.Web.Http.Authorize]
    public class GroupBlobsController : ApiController
    {
        private Entities db = new Entities();

        // GET: api/GroupBlobs
        public IQueryable<GroupBlob> GetGroupBlobs()
        {
            return db.GroupBlobs;
        }

        // GET: api/GroupBlobs/5
        [ResponseType(typeof(GroupBlob))]
        public IHttpActionResult GetGroupBlob(string id)
        {
            GroupBlob groupBlob = db.GroupBlobs.Find(id);
            if (groupBlob == null)
            {
                return NotFound();
            }

            return Ok(groupBlob);
        }

        // PUT: api/GroupBlobs/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutGroupBlob(string id, GroupBlob groupBlob)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != groupBlob.id)
            {
                return BadRequest();
            }

            db.Entry(groupBlob).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!GroupBlobExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/GroupBlobs
        [ResponseType(typeof(GroupBlob))]
        public IHttpActionResult PostGroupBlob(GroupBlob groupBlob)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.GroupBlobs.Add(groupBlob);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (GroupBlobExists(groupBlob.id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = groupBlob.id }, groupBlob);
        }

        // DELETE: api/GroupBlobs/5
        [ResponseType(typeof(GroupBlob))]
        public IHttpActionResult DeleteGroupBlob(string id)
        {
            GroupBlob groupBlob = db.GroupBlobs.Find(id);
            if (groupBlob == null)
            {
                return NotFound();
            }

            db.GroupBlobs.Remove(groupBlob);
            db.SaveChanges();

            return Ok(groupBlob);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool GroupBlobExists(string id)
        {
            return db.GroupBlobs.Count(e => e.id == id) > 0;
        }
    }
}