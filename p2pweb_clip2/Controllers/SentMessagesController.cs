﻿using p2pweb_clip2.Models.dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using p2pweb_clip2.Models.edm;

namespace p2pweb_clip2.Controllers
{
    [RequireHttps]
    [System.Web.Http.Authorize]
    public class SentMessagesController : ApiController
    {
        // POST api/<controller>
        public void Post([FromBody]SentMessageDTO dto)
        {
            if (User.Identity.Name == null)
                return;

            using (var db = new Entities())
            {
                var dbrec = new SentMessage()
                {
                    rcpt_fbid = dto.rcpt_fbid
                     ,
                    sent_date = DateTime.Now
                     ,
                    username = User.Identity.Name
                };

                db.SentMessages.Add(dbrec);

                db.SaveChanges();
            }
            

        }
    }
}
