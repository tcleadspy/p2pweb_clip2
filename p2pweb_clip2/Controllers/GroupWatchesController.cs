﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using p2pweb;
using p2p_web_ng;
using p2pweb_clip2;
using System.Web.Mvc;
using p2pweb_clip2.Models.dto;
using p2pweb_clip2.Models.edm;


namespace p2pweb.Controllers
{
    [RequireHttps]
    [System.Web.Http.Authorize]
    public class GroupWatchesController : ApiController
    {
        private Entities db = new Entities();

        public List<FBPageJsonDTO> Get()
        {
            var qry =
                from
                    gw1 in db.GroupWatches
                    join feed in db.FeedBlobs on gw1.feedid equals feed.id
                where
                    gw1.username == User.Identity.Name
                select
                    new FBPageJsonDTO()
                    {
                        id = feed.id
                        ,objJson = feed.obj
                    };

            var watches = qry.ToList();

            return watches;
        }

        // POST api/<controller>
        public void Post([FromBody]FBPageJsonDTO page)
        {

            if (User.Identity.Name == null)
                return;

            var existing =
                db
                .GroupWatches
                .Any((gw) => gw.feedid == page.id && gw.username == User.Identity.Name);

            if (existing)
                return;

            var existingFeed =
                db.FeedBlobs.Any((f) => f.id == page.id);

            using (var db = new Entities())
            {
                if (!existingFeed)
                {

                    var newFeedBlob = new FeedBlob()
                    {
                        id = page.id
                         , obj = page.objJson
                    };

                    db.FeedBlobs.Add(newFeedBlob);
                }
                else
                {
                    var existingBlob = db.FeedBlobs.Single((f) => f.id == page.id);
                    existingBlob.obj = page.objJson;
                }

                var newgroupwatch = new GroupWatch()
                {
                    feedid = page.id
                     ,
                    username = User.Identity.Name
                };

                db.GroupWatches.Add(newgroupwatch);

                db.SaveChanges();
            }
        }

        public void Delete([FromUri]string id)
        {
            using (var db = new Entities())
            {
                var objs = db.GroupWatches
                    .Where((p) => p.feedid == id && p.username == User.Identity.Name);


                foreach (var obj in objs)
                {
                    db.GroupWatches.Remove(obj);
                }

                db.SaveChanges();
            }
        }

        //// GET: api/GroupWatches
        //public IQueryable<GroupWatch> GetGroupWatches()
        //{
        //    return db.GroupWatches;
        //}

        //// GET: api/GroupWatches/5
        //[ResponseType(typeof(GroupWatch))]
        //public IHttpActionResult GetGroupWatch(long id)
        //{
        //    GroupWatch groupWatch = db.GroupWatches.Find(id);
        //    if (groupWatch == null)
        //    {
        //        return NotFound();
        //    }

        //    return Ok(groupWatch);
        //}

        //// PUT: api/GroupWatches/5
        //[ResponseType(typeof(void))]
        //public IHttpActionResult PutGroupWatch(long id, GroupWatch groupWatch)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    if (id != groupWatch.id)
        //    {
        //        return BadRequest();
        //    }

        //    db.Entry(groupWatch).State = EntityState.Modified;

        //    try
        //    {
        //        db.SaveChanges();
        //    }
        //    catch (DbUpdateConcurrencyException)
        //    {
        //        if (!GroupWatchExists(id))
        //        {
        //            return NotFound();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }

        //    return StatusCode(HttpStatusCode.NoContent);
        //}

        //// POST: api/GroupWatches
        //[ResponseType(typeof(GroupWatch))]
        //public IHttpActionResult PostGroupWatch(GroupWatch groupWatch)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    db.GroupWatches.Add(groupWatch);
        //    db.SaveChanges();

        //    return CreatedAtRoute("DefaultApi", new { id = groupWatch.id }, groupWatch);
        //}

        //// DELETE: api/GroupWatches/5
        //[ResponseType(typeof(GroupWatch))]
        //public IHttpActionResult DeleteGroupWatch(long id)
        //{
        //    GroupWatch groupWatch = db.GroupWatches.Find(id);
        //    if (groupWatch == null)
        //    {
        //        return NotFound();
        //    }

        //    db.GroupWatches.Remove(groupWatch);
        //    db.SaveChanges();

        //    return Ok(groupWatch);
        //}

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        db.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}

        //private bool GroupWatchExists(long id)
        //{
        //    return db.GroupWatches.Count(e => e.id == id) > 0;
        //}
    }
}